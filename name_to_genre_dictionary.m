function genreDict = name_to_genre_dictionary()
    % Define the names of the pieces and their corresponding genres
    pieceNames = {'BachSonata3Fugue_mid','BachSuite3Allemande_mid','BachSuite3Bourree_mid','Aerodynamic_mid','Contact_mid','Derezzed_mid','DigitalLove_mid','GiorgioByMoroder_mid','HarderBetterFasterStronger_mid','ShortCircuit_mid','VeridisQuo_mid','BlackPearls_mid','ChameleonSaxRiff_mid','ChameleonBassRiff_mid','IMeanYouMainMelody_mid','OneFingerSnap_mid','HotelCalifornia_mid','WhiteWedding_mid','letsdance_mid','SweetChildOMine_mid'}; % Add more names
    genres = {'Bach', 'Bach', 'Bach', 'DaftPunk', 'DaftPunk', 'DaftPunk', 'DaftPunk', 'DaftPunk', 'DaftPunk', 'DaftPunk', 'DaftPunk', 'Jazz_Pop', 'Jazz_Pop', 'Jazz_Pop', 'Jazz_Pop', 'Jazz_Pop', 'Jazz_Pop', 'Jazz_Pop', 'Jazz_Pop', 'Jazz_Pop'}; % Corresponding genres
    
    % Create the dictionary
    genreDict = containers.Map(pieceNames, genres);
end
