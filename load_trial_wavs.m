function wavs = load_trial_wavs(basePath,downfs,subNum)
    nTrials = 30;
    wavs = {};
    for i = 1:nTrials
        wavPath = strcat(basePath,'/subject_',int2str(subNum),'_trial_',int2str(i-1),'.wav');
        [y,Fs] = audioread(wavPath);
        envelope = mTRFenvelope(y(:,1));
        envelope = resample(envelope,downfs,Fs);
        wavs = [wavs,envelope];
    end
end