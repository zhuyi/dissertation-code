%%
names = ["Subject_1","Subject_2","Subject_3","Subject_4","Subject_5","Subject_6","Subject_7","Subject_8","Subject_9","Subject_10"];
nSubs = length(names);
wav_fs = 64;
EEG_fs = 512;
%%
for i = 1:nSubs
    subName = names(i);
    disp(subName)
    % Add libs to the path
    addpath("eeglab2023.1/plugins/BDFimport1.2/");
    addpath("mTRF-Toolbox/mtrf/")
    
    % Load EEG and trial envelopes
    
    EEGfilename = "../Data/eeg/subject"+i+".bdf";
    wavsBasePath = "../Data/stim/subject_"+i+"/bad_removed";
    jsonPath = "../melody-expectation-experiment/stimuli/subjects/"+subName+"/good_blocks.json";
    
    %%
    [EEG_Sections,Mastoid_Sections,songChanges] = load_EEG_data(EEGfilename);
    disp('Done loading EEG data!')
    
    %Fix sections with bad stimuli
    fixed_EEGs = fix_eegs(EEG_Sections,jsonPath,songChanges);
    fixed_Mastoids = fix_eegs(Mastoid_Sections,jsonPath,songChanges);
    %
    %%
    disp("Loading wavs")
    wavs = load_trial_wavs(wavsBasePath,wav_fs,i);
    disp('Done loading wavs!')
    %% Set EEG to the same length as the trials
    short_EEG_Sections = set_EEG_lengths(fixed_EEGs,wavs,EEG_fs/wav_fs);
    short_Mastoid_Sections = set_EEG_lengths(fixed_Mastoids,wavs,EEG_fs/wav_fs);
    %%
    
    % Load onsets
    trialOnsetsPath = "../Data/onsets/subject_"+i+"_note_onsets.json";
    % Get trial lengths and derivatives
    lengths = {};
    derivatives = {};
    fluxes = {};
    for j = 1:30
        lengths = [lengths,length(wavs{1,j})];
        derivatives = [derivatives,diff(wavs{1,j})];
        fluxes = [fluxes,spectralFlux(wavs{1,j},wav_fs)];
    end

    derivatives = set_EEG_lengths(derivatives,wavs,1);
    fluxes = set_EEG_lengths(fluxes,wavs,1);
    
    [trialOnsets,altEnvelopes] = load_trial_onsets(trialOnsetsPath,lengths,wav_fs);
    trialOnsets = set_EEG_lengths(trialOnsets,wavs,1);
    disp('Done loading onsets!')
    
    %%
    % Generate the eeg data structure
    eeg = {};
    eeg.data = short_EEG_Sections;
    ext = {};
    ext.data = short_Mastoid_Sections;
    ext.description = 'Mastoids';
    eeg.extChan = {ext};
    eeg.dataType = 'EEG';
    eeg.deviceName = 'BioSemi';
    eeg.fs = 512;
    chanlocs = load('chanlocs64.mat');
    eeg.chanlocs = {};
    eeg.chanlocs = chanlocs.chanlocs;
    disp("Saving EEG")
    save("../Data/dataCND/dataSub"+i+".mat","eeg");
    % Generate the stim data structure
    stim = {};
    data = cat(1,wavs,trialOnsets,derivatives,fluxes,altEnvelopes);
    stim.data = data;
    stim.names = {'music envelope vectors','note onsets','music envelope derivatives','envelope spectral flux','constructed envelope'};
    stim.fs = wav_fs;
    disp("Saving Stim")
    save("../Data/dataCND/dataStim"+i+".mat","stim");
    disp("Done!")

end