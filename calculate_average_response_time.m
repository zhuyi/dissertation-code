% Define the directories
rootDir = fullfile('..', 'melody-expectation-experiment');
stimuliDir = fullfile(rootDir, 'stimuli/subjects');
behavioralDir = fullfile(rootDir, 'behavioural');
outputFile = fullfile('..', 'Code', 'average_response_times.json');

% Load the switch times data
switchTimesFile = fullfile('Dissertation/Code', 'switch_times.json');
if ~exist(switchTimesFile, 'file')
    error('Switch times file not found.');
end
switchTimesData = jsondecode(fileread(switchTimesFile));

% Get a list of all subject folders
subjectFolders = dir(fullfile(stimuliDir, 'subject_*'));

% Initialize a structure to store the average response times for all subjects
allSubjectsResponseTimes = struct();
allResponseTimes = []; % To accumulate all response times for overall average

% Loop through each subject folder
for subjectIdx = 1:length(subjectFolders)
    subjectName = subjectFolders(subjectIdx).name;
    subjectFolder = fullfile(behavioralDir, subjectName);
    buttonPressesFile = fullfile(subjectFolder, 'buttonPresses.json');
    
    % Check if the buttonPresses.json file exists
    if ~exist(buttonPressesFile, 'file')
        warning('buttonPresses.json file not found for %s', subjectName);
        continue;
    end
    
    % Read the buttonPresses.json file
    buttonPressesData = jsondecode(fileread(buttonPressesFile));
    
    % Initialize an array to store response times for the current subject
    responseTimes = [];
    
    % Get the switch times for the current subject
    if ~isfield(switchTimesData, subjectName)
        warning('No switch times found for %s', subjectName);
        continue;
    end
    subjectSwitchTimes = switchTimesData.(subjectName);
    
    % Loop through each block
    blockNames = fieldnames(subjectSwitchTimes);
    for blockIdx = 1:length(blockNames)
        blockName = blockNames{blockIdx};
        
        % Check if the block exists in the button presses data
        if ~isfield(buttonPressesData, blockName)
            warning('No button presses found for block %s in %s', blockName, subjectName);
            continue;
        end
        
        % Loop through each trial in the block
        trialNames = fieldnames(subjectSwitchTimes.(blockName));
        for trialIdx = 1:length(trialNames)
            trialName = trialNames{trialIdx};
            
            % Get the switch times for the current trial
            switches = subjectSwitchTimes.(blockName).(trialName);
            
            % Get the button presses for the current trial
            if ~isfield(buttonPressesData.(blockName), trialName)
                warning('No button presses found for trial %s in block %s in %s', trialName, blockName, subjectName);
                continue;
            end
            buttonPresses = buttonPressesData.(blockName).(trialName);
            
            % Loop through each switch time
            for switchIdx = 1:length(switches)
                switchTime = switches(switchIdx);
                
                % Find the first button press within 5 seconds after the switch
                pressIdx = find(buttonPresses > switchTime & buttonPresses <= switchTime + 5, 1);
                if ~isempty(pressIdx)
                    responseTime = buttonPresses(pressIdx) - switchTime;
                    responseTimes(end+1) = responseTime; % Calculate the response time
                    allResponseTimes(end+1) = responseTime; % Accumulate for overall average
                end
            end
        end
    end
    
    % Calculate the average response time for the current subject
    if ~isempty(responseTimes)
        avgResponseTime = mean(responseTimes);
    else
        avgResponseTime = NaN; % No valid response times found
    end
    
    % Store the average response time for the current subject
    allSubjectsResponseTimes.(subjectName) = avgResponseTime;
end

% Calculate the overall average response time across all subjects
if ~isempty(allResponseTimes)
    overallAvgResponseTime = mean(allResponseTimes);
else
    overallAvgResponseTime = NaN;
end

% Store the overall average response time
allSubjectsResponseTimes.OverallAverage = overallAvgResponseTime;

% Save the average response times to a JSON file
jsonStr = jsonencode(allSubjectsResponseTimes);
fid = fopen(outputFile, 'w');
if fid == -1
    error('Cannot create JSON file');
end
fwrite(fid, jsonStr, 'char');
fclose(fid);

disp('Average response times recorded successfully.');
disp(['Overall average response time: ', num2str(overallAvgResponseTime), ' seconds']);