% Define the root directory where the subject folders are located
rootDir = fullfile('..', 'melody-expectation-experiment', 'stimuli', 'subjects');

% Define the output file
outputFile = fullfile('..', 'Code', 'switch_times.json');

% Get a list of all subject folders
subjectFolders = dir(fullfile(rootDir, 'subject_*'));

% Initialize a structure to store the switch times for all subjects
allSubjectsSwitchTimes = struct();

% Loop through each subject folder
for subjectIdx = 1:length(subjectFolders)
    subjectFolder = fullfile(rootDir, subjectFolders(subjectIdx).name);
    blocksFile = fullfile(subjectFolder, 'blocks.json');
    
    % Check if the blocks.json file exists
    if ~exist(blocksFile, 'file')
        warning('blocks.json file not found for %s', subjectFolders(subjectIdx).name);
        continue;
    end
    
    % Read the blocks.json file
    blocksData = jsondecode(fileread(blocksFile));
    
    % Initialize a structure to store switch times for the current subject
    subjectSwitchTimes = struct();
    
    % Loop through each block
    blockNames = fieldnames(blocksData);
    for blockIdx = 1:length(blockNames)
        block = blocksData.(blockNames{blockIdx});
        
        % Initialize a structure to store switch times for the current block
        blockSwitchTimes = struct();
        
        % Loop through each trial in the block
        trialNames = fieldnames(block);
        for trialIdx = 1:length(trialNames)
            trial = block.(trialNames{trialIdx});
            
            % Check if trial is a structure
            if ~isstruct(trial)
                warning('Unexpected trial format for %s in %s', trialNames{trialIdx}, blockNames{blockIdx});
                continue;
            end
            
            % Initialize an array to store switch times for the current trial
            switchTimes = [];
            
            % Initialize the cumulative time to zero
            cumulativeTime = 0;
            
            % Loop through each segment in the trial
            segmentNames = fieldnames(trial);
            for segmentIdx = 1:length(segmentNames)
                segment = trial.(segmentNames{segmentIdx});
                
                % Check if segment is a structure
                if ~isstruct(segment)
                    warning('Unexpected segment format for %s in %s in %s', segmentNames{segmentIdx}, trialNames{trialIdx}, blockNames{blockIdx});
                    continue;
                end
                
                % Increment the cumulative time by the segment length
                cumulativeTime = cumulativeTime + segment.segment_length / 1000; % Convert to seconds
                
                % If this is not the last segment, record the switch time
                if segmentIdx < length(segmentNames)
                    switchTimes(end+1) = cumulativeTime;
                end
            end
            
            % Store the switch times for the current trial
            blockSwitchTimes.(trialNames{trialIdx}) = switchTimes;
        end
        
        % Store the switch times for the current block
        subjectSwitchTimes.(blockNames{blockIdx}) = blockSwitchTimes;
    end
    
    % Store the switch times for the current subject
    allSubjectsSwitchTimes.(subjectFolders(subjectIdx).name) = subjectSwitchTimes;
end

% Save the switch times to a JSON file
jsonStr = jsonencode(allSubjectsSwitchTimes);
fid = fopen(outputFile, 'w');
if fid == -1
    error('Cannot create JSON file');
end
fwrite(fid, jsonStr, 'char');
fclose(fid);

disp('Switch times recorded successfully.');