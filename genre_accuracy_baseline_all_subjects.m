names = {"Subject_1", "Subject_10", "Subject_2", "Subject_3", "Subject_4", "Subject_5", "Subject_6", "Subject_7", "Subject_8", "Subject_9"};

% Initialize a cell array to store accuracies for each subject across all iterations
totalAccuracies = cell(1, length(names));

for j = 1:100
    for i = 1:length(names)
        accuracy = genre_accuracy_baseline(allTests{i}, genreDict);
        totalAccuracies{i} = [totalAccuracies{i} accuracy];
    end
end

% Calculate and display average accuracies for each subject
averageAccuracies = cellfun(@mean, totalAccuracies);
disp('Average Accuracies for each subject:');
disp(averageAccuracies);
