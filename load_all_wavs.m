function wavs = load_all_wavs()
wavPianoLocations = '../Data/stim/pilot_stimuli/components/Piano/**/';
wavSaxLocations = '../Data/stim/pilot_stimuli/components/Sax/**/';

pianoFilenames = dir([wavPianoLocations,'*.wav']);
saxFilenames = dir([wavSaxLocations,'*.wav']);

wavs = {};

for i = 1:length(pianoFilenames)
    [wav,Fs] = audioread([pianoFilenames(i).folder,'/',pianoFilenames(i).name]);
    %wav = abs(hilbert(wav(:,1)));
    wav = wav(:,1);
    wav = resample(wav,64,Fs);
    pwavstruct = {};
    pwavstruct.data = wav;
    pwavstruct.name = pianoFilenames(i).name;
    pwavstruct.instrument = 'Piano';

    wavs = [wavs,pwavstruct];
    
    if i ~= 20
        [wav,Fs] = audioread([saxFilenames(i).folder,'/',saxFilenames(i).name]);
        %wav = abs(hilbert(wav(:,1)));
        wav = wav(:,1);
        wav = resample(wav,64,Fs);
        pwavstruct = {};
        pwavstruct.data = wav;
        pwavstruct.name = saxFilenames(i).name;
        pwavstruct.instrument = 'Sax';
        
        wavs = [wavs,pwavstruct];
    end
end

end