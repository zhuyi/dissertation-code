% Load the standard deviation data
load('std_deviation_EEG_around_switches.mat', 'stdDeviationData');

% Number of subjects, electrodes, and latencies
numSubjects = length(stdDeviationData);
numLatencies = 641; % prePostSamples * 2 + 1

% Initialize a matrix to accumulate the standard deviation data for averaging
allSubjectsStdDeviation = zeros(numSubjects, numLatencies);

% Collect data from each subject
for subjectIndex = 1:10
    % Extract the standard deviation data for the current subject
    subjectStdDeviation = stdDeviationData(subjectIndex).subjectStdDev;
    
    % Ensure the subject data is in the expected format
    if length(subjectStdDeviation) ~= numLatencies
        error('Unexpected data format for subject %d. Expected %d latencies, found %d.', ...
              subjectIndex, numLatencies, length(subjectStdDeviation));
    end
    
    % Store the standard deviation data in the matrix
    allSubjectsStdDeviation(subjectIndex, :) = subjectStdDeviation;
end

% Calculate the average standard deviation across all subjects
averageStdDeviation = mean(allSubjectsStdDeviation, 1);

% Define time vector for plotting (assuming -5 to +5 seconds)
timeVector = linspace(-5, 5, numLatencies);

% Time points of interest (in seconds)
highlightTimes = [0.3281, 0.6406, 1.3125];

% Find the indices of the specified time points
highlightIndices = arrayfun(@(t) find(abs(timeVector - t) == min(abs(timeVector - t)), 1), highlightTimes);

% Get the values corresponding to the highlight indices
highlightValues = averageStdDeviation(highlightIndices);

% Average response time of all subjects (in seconds)
averageResponseTime = 1.3856;

% Plot the average standard deviation and mark the specified points
figure;
plot(timeVector, averageStdDeviation, 'LineWidth', 2);
hold on;
plot(highlightTimes, highlightValues, 'ro', 'MarkerSize', 10, 'LineWidth', 2);
xline(averageResponseTime, 'k--', 'LineWidth', 2); % Draw vertical line at average response time
xlabel('Time (seconds)');
ylabel('Global Field Power');
grid on;
xticks(linspace(-5, 5, 11)); % Set ticks at -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5
legend('Average Std Dev', 'Highlighted Peaks', 'Avg Response Time', 'Location', 'northwest');
hold off;

% Save the figure in the plots folder
plotsFolder = 'EEG_StdDeviation_Plots';
if ~exist(plotsFolder, 'dir')
    mkdir(plotsFolder);
end
averagePlotFileName = fullfile(plotsFolder, 'Average_EEG_StdDeviation_with_HighlightedPoints.png');
saveas(gcf, averagePlotFileName);

% Close the figure to avoid too many open figures
close(gcf);

disp('Plot with highlighted points and average response time created and saved successfully.');
