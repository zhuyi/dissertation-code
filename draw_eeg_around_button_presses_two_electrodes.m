% Load the extracted data
load('extracted_Fz_Pz_EEG_around_button_presses.mat', 'extractedData');

% Number of subjects, electrodes, and samples
numSubjects = length(extractedData);
numElectrodes = 2; % Fz and Pz
numSamples = size(extractedData(1).subjectData{1,1}, 1);

% Initialize matrices to store the average response for Fz and Pz
averageResponseFz = zeros(numSamples, 1);
averageResponsePz = zeros(numSamples, 1);

% Loop through each subject and trial to accumulate the EEG data
for subjectIndex = 1:numSubjects
    for trialIndex = 1:size(extractedData(subjectIndex).subjectData, 2)
        % Add up the responses for Fz and Pz
        averageResponseFz = averageResponseFz + mean(extractedData(subjectIndex).subjectData{1, trialIndex}, 2);
        averageResponsePz = averageResponsePz + mean(extractedData(subjectIndex).subjectData{2, trialIndex}, 2);
    end
end

% Divide by the total number of subjects and trials to get the average
numTrials = size(extractedData(1).subjectData, 2);
averageResponseFz = averageResponseFz / (numSubjects * numTrials);
averageResponsePz = averageResponsePz / (numSubjects * numTrials);

% Define time vector for plotting, focusing on -4 to 2 seconds
fs = 64; % Sampling frequency
prePostSec = 5;
timeVector = linspace(-prePostSec, prePostSec, numSamples);
timeRange = [-4, 2]; % Define the time range of interest
timeIndices = find(timeVector >= timeRange(1) & timeVector <= timeRange(2));

% Plot the average responses for Fz and Pz within the desired time range
figure;
plot(timeVector(timeIndices), averageResponseFz(timeIndices), 'r', 'LineWidth', 2); hold on;
plot(timeVector(timeIndices), averageResponsePz(timeIndices), 'b', 'LineWidth', 2);
xlabel('Time (seconds)');
ylabel('Amplitude (\muV)');
legend('Fz', 'Pz');
grid on;

% Save the figure
saveas(gcf, 'Average_EEG_Response_Fz_Pz_-4_to_2.png');

disp('Average EEG responses for Fz and Pz electrodes from -4 to 2 seconds plotted and saved successfully.');
