function [EEG_Sections,mastoid_sections,changes] = load_EEG_data(filename)
    NEUTRAL = 0;
    STIMULUS_START = 1;
    SONG_CHANGE = 2;
    BUTTON_PRESS = 3;

    addpath('./eeglab2023.1/plugins/BDFimport1.2/');

    [EEG_raw, trigs] = Read_bdf(filename);
    EEG_raw = EEG_raw(1:66,:); % 64 elec + 2 mastoids

    disp('Loaded')

    % Getting triggers
    trigs=trigs-min(trigs);
    trigs(trigs>256) = trigs(trigs>256)-min(trigs(trigs>256));

    trialStarts = [find(diff(trigs==65536)) find(diff(trigs==STIMULUS_START))];
    trialStarts = trialStarts(1:2:end);
    songChanges = find(diff(trigs==SONG_CHANGE));
    songChanges = songChanges(1:2:end);

    others = find(trigs);
    
    %End of trial will be denoted by every 6th song change
    trialEnds = songChanges(6:6:end);
    
    nTrials = length(trialStarts);
    
    EEG_Sections = {};
    mastoid_sections = {};
    changes = {};
    
    for i = 1:nTrials
        start = trialStarts(i);
        fin = trialEnds(i);
        EEG_Sections = [EEG_Sections,transpose(EEG_raw(1:64,start:fin))];
        mastoid_sections = [mastoid_sections,transpose(EEG_raw(65:66,start:fin))];
        changes = [changes,songChanges((6*(i-1))+1:(6*(i-1))+6)-start];
    end

    disp(length(EEG_Sections))

end