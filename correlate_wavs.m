function corrs = correlate_wavs(eegSection,model,wavs)
    %CORRELATE_WAVS correlates eegSection against all of the wavs present
    %in wavs according to the bTRF model in model

    corrs = {};

    % change to for here if you dont have parallel computing toolbox
    parfor i = 1:length(wavs)
        windowYPred = mTRFpredict([],eegSection,model,'verbose',0);
        corr = sliding_window_correlations(windowYPred,wavs{i}.data,model,64);
        corrs{i} = corr;
    end

end

