function acc = genre_accuracy_baseline(ansArr, genreDict)
    top_5_genre_count = 0;
    for i = 1:length(ansArr)
        expected_name = ansArr{i}.expected;
        expected_name = expected_name(find(~isspace(expected_name)));
        expected_name = strsplit(expected_name,".");
        expected_name = strcat(expected_name(1),"_mid");
        expected_genre = genreDict(expected_name); % Get the genre of the expected piece

        ranks = ansArr{i}.ranks; % Access the full array of ranks
        indices = randperm(20,5); % Generate 5 unique random indices from 1 to 20
        randomRanks = ranks(indices); % Select elements at these indices

        % Create an array of genres for the top 5 predictions
        predicted_genres = arrayfun(@(x) genreDict(x.name), randomRanks, 'UniformOutput', false);

        % Count the occurrence of each genre in the top 5 predictions
        genre_counts = groupcounts(categorical(predicted_genres));

        % Determine the majority genre
        [maxCount, idx] = max(genre_counts);
        majority_genres = predicted_genres(genre_counts == maxCount);

        % If there's a tie, select based on the highest-ranked score
        predicted_genre = majority_genres{1};
        if length(majority_genres) > 1
            % Find which of the tied genres has the highest score in 'ranks'
            tied_scores = cellfun(@(x) find(strcmp({randomRanks(:).name}, x)), majority_genres, 'UniformOutput', false);
            scores = cellfun(@(x) randomRanks(x).score, tied_scores, 'UniformOutput', false);
            [~, highest_score_idx] = max(cell2mat(scores));
            predicted_genre = majority_genres{highest_score_idx};
        end

        % Compare the predicted genre with the expected genre
        if strcmp(predicted_genre, expected_genre)
            top_5_genre_count = top_5_genre_count + 1;
        end
    end

    % Calculate accuracy
    acc = top_5_genre_count / length(ansArr);
end
