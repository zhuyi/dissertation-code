
function acc = top_5_accuracy(ansArr)
    top_5_count = 0;
    for i = 1:length(ansArr)
        expected = ansArr{i}.expected;
        expected = expected(find(~isspace(expected)));
        expected = strsplit(expected,".");
        expected = strcat(expected(1),"_mid");
        ranks = ansArr{i}.ranks(1:5);
        
        if (ismember(expected,{ranks(:).name}))
            top_5_count = top_5_count +1;
        end
    end
    
    %disp(top_5_count/length(ansArr))
    acc = top_5_count/length(ansArr);
end