% Define the root directories for the data
dataDir = fullfile('..', 'Data', 'dataCND');
behaviouralDir = fullfile('..', 'melody-expectation-experiment', 'behavioural');
stimuliDir = fullfile('..', 'melody-expectation-experiment', 'stimuli', 'subjects');
outputDir = fullfile('..', 'Code');

% Sampling frequency
fs = 64;

for subjectIndex = 1:10
    % Load the pre_dataStim.mat file for the current subject
    stimFilePath = fullfile(dataDir, ['pre_dataStim', num2str(subjectIndex), '.mat']);
    if exist(stimFilePath, 'file')
        load(stimFilePath); % Loads the 'stim' structure
    else
        disp(['File does not exist: ', stimFilePath]);
        continue; % Skip this subject if the file doesn't exist
    end

    % Load the pre_dataSub.mat file for the current subject
    subFilePath = fullfile(dataDir, ['pre_dataSub', num2str(subjectIndex), '.mat']);
    if exist(subFilePath, 'file')
        load(subFilePath); % Loads the 'sub' structure
    else
        disp(['File does not exist: ', subFilePath]);
        continue; % Skip this subject if the file doesn't exist
    end

    % Load the blocks.json file for the current subject
    jsonFilePath = fullfile(stimuliDir, ['subject_', num2str(subjectIndex)], 'blocks.json');
    if exist(jsonFilePath, 'file')
        jsonText = fileread(jsonFilePath);
        blocksData = jsondecode(jsonText);
    else
        disp(['File does not exist: ', jsonFilePath]);
        continue; % Skip this subject if the file doesn't exist
    end

    % Initialize containers for genre and non-genre switches
    genreSwitchData = [];
    nonGenreSwitchData = [];
    
    % Process each block and trial contained in the blocks.json
    blockNames = fieldnames(blocksData);
    for blockIndex = 1:length(blockNames)
        blockName = blockNames{blockIndex};
        currentBlock = blocksData.(blockName);

        % Exclude the 'instrument' field
        trialNames = fieldnames(currentBlock);
        trialNames(strcmp(trialNames, 'instrument')) = [];
        
        for trialIndex = 1:length(trialNames)
            trialName = trialNames{trialIndex};
            trialData = currentBlock.(trialName);
            
            % Get the list of segments in the trial
            segmentNames = fieldnames(trialData);
            previousGenre = '';
            
            for segIndex = 1:length(segmentNames)
                segment = trialData.(segmentNames{segIndex});
                currentGenre = segment.genre;
                
                if segIndex > 1 % If it's not the first segment
                    % Determine if it's a genre switch or non-genre switch
                    if strcmp(currentGenre, previousGenre)
                        % Non-genre switch
                        nonGenreSwitchData = [nonGenreSwitchData; {subjectIndex, blockName, trialName, segIndex, segment}];
                    else
                        % Genre switch
                        genreSwitchData = [genreSwitchData; {subjectIndex, blockName, trialName, segIndex, segment}];
                    end
                end
                
                % Update the previous genre
                previousGenre = currentGenre;
            end
        end
    end

    % Save the identified switches to new files
    save(fullfile(outputDir, ['subject_', num2str(subjectIndex), '_genre_switches.mat']), 'genreSwitchData');
    save(fullfile(outputDir, ['subject_', num2str(subjectIndex), '_non_genre_switches.mat']), 'nonGenreSwitchData');
end

disp('Genre and non-genre switches identified and saved successfully.');
