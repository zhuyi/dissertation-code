% Load the standard deviation data
load('std_deviation_EEG_around_switches.mat', 'stdDeviationData');

% Number of subjects, electrodes, and latencies
numSubjects = length(stdDeviationData);
numLatencies = 641; % prePostSamples * 2 + 1

% Create a folder for the plots if it doesn't exist
plotsFolder = 'EEG_StdDeviation_Plots';
if ~exist(plotsFolder, 'dir')
    mkdir(plotsFolder);
end

% Initialize a matrix to accumulate the standard deviation data for averaging
allSubjectsStdDeviation = zeros(numSubjects, numLatencies);

% Define time vector for plotting (assuming -5 to +5 seconds)
timeVector = linspace(-5, 5, numLatencies);

% Plot individual standard deviation graphs for each subject and collect data
figure;
hold on; % Hold the plot to overlay multiple lines
for subjectIndex = 1:10
    % Extract the standard deviation data for the current subject
    subjectStdDeviation = stdDeviationData(subjectIndex).subjectStdDev;
    
    % Ensure the subject data is in the expected format
    if length(subjectStdDeviation) ~= numLatencies
        error('Unexpected data format for subject %d. Expected %d latencies, found %d.', ...
              subjectIndex, numLatencies, length(subjectStdDeviation));
    end
    
    % Store the standard deviation data in the matrix
    allSubjectsStdDeviation(subjectIndex, :) = subjectStdDeviation;
    
    % Plot the standard deviation across latencies for the current subject
    plot(timeVector, subjectStdDeviation, 'LineWidth', 1);
end

% Customize the plot
xlabel('Time (seconds)');
ylabel('Global Field Power');
grid on;
xticks(linspace(-5, 5, 11)); % Set ticks at -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5

% Save the combined figure in the plots folder
combinedPlotFileName = fullfile(plotsFolder, 'Combined_EEG_StdDeviation_Switches.png');
saveas(gcf, combinedPlotFileName);

% Close the figure to avoid too many open figures
close(gcf);
