% Load the averaged data
load('averaged_EEG_around_switches.mat', 'averagedData');

% Number of subjects, electrodes, and latencies
numSubjects = length(averagedData);
numElectrodes = 64;
numLatencies = 641; % prePostSamples * 2 + 1

% Initialize the new structure for storing the standard deviation data
stdDeviationData = struct('subjectStdDev', cell(1, numSubjects));

% Loop over each subject
for subjectIndex = 1:10
    % Extract the averaged data for the current subject
    subjectAveragedData = averagedData(subjectIndex).subjectData;
    
    % Calculate the standard deviation across electrodes for each latency
    subjectStdDev = std(subjectAveragedData, 0, 1);
    
    % Store the standard deviation data for the current subject
    stdDeviationData(subjectIndex).subjectStdDev = subjectStdDev;
end

% Save the standard deviation data to a new .mat file
save('std_deviation_EEG_around_switches.mat', 'stdDeviationData');
