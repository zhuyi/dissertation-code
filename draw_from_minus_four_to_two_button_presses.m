% Load the standard deviation data
load('std_deviation_EEG_around_button_presses.mat', 'stdDeviationData');

% Number of subjects, electrodes, and latencies
numSubjects = length(stdDeviationData);
numLatencies = 641; % prePostSamples * 2 + 1
fs = 64; % Sampling frequency
prePostSec = 5; % Pre and post seconds

% Initialize a matrix to accumulate the standard deviation data for averaging
allSubjectsStdDeviation = zeros(numSubjects, numLatencies);

% Collect data from each subject
for subjectIndex = 1:10
    % Extract the standard deviation data for the current subject
    subjectStdDeviation = stdDeviationData(subjectIndex).subjectStdDev;
    
    % Ensure the subject data is in the expected format
    if length(subjectStdDeviation) ~= numLatencies
        error('Unexpected data format for subject %d. Expected %d latencies, found %d.', ...
              subjectIndex, numLatencies, length(subjectStdDeviation));
    end
    
    % Store the standard deviation data in the matrix
    allSubjectsStdDeviation(subjectIndex, :) = subjectStdDeviation;
end

% Define time vector for plotting (assuming -5 to +5 seconds)
timeVector = linspace(-5, 5, numLatencies);

% Focus on the time range from -4 to 2 seconds
timeRange = [-4, 2];
timeIndices = find(timeVector >= timeRange(1) & timeVector <= timeRange(2));
timeVectorFocused = timeVector(timeIndices);

% Plot individual standard deviation graphs for each subject in the focused time range
figure;
hold on; % Hold the plot to overlay multiple lines
for subjectIndex = 1:10
    % Extract the standard deviation data for the current subject in the focused time range
    subjectStdDeviation = allSubjectsStdDeviation(subjectIndex, timeIndices);
    
    % Plot the standard deviation across latencies for the current subject
    plot(timeVectorFocused, subjectStdDeviation, 'LineWidth', 1.5);
end

% Add a vertical line at 0 seconds
xline(0, '--k', 'LineWidth', 1.5); % Dashed black line at 0 seconds

% Customize the plot
xlabel('Time (seconds)');
ylabel('Global Field Power');
grid on;

% Ensure 0 seconds is included in the x-axis ticks
xticks(linspace(timeRange(1), timeRange(2), 11)); % Set ticks at the specified time range
xticks(sort([xticks, 0])); % Ensure 0 is included in the x-ticks

% Save the combined figure in the plots folder
plotsFolder = 'EEG_StdDeviation_Plots_ButtonPresses';
if ~exist(plotsFolder, 'dir')
    mkdir(plotsFolder);
end
combinedPlotFileName = fullfile(plotsFolder, 'Combined_EEG_StdDeviation_from_-4_to_2.png');
saveas(gcf, combinedPlotFileName);

% Close the figure to avoid too many open figures
close(gcf);

% Calculate the average standard deviation across all subjects in the focused time range
averageStdDeviationFocused = mean(allSubjectsStdDeviation(:, timeIndices), 1);

% Highlight the peaks at 0 and 0.3125 seconds
highlightTimes = [0, 0.3125];
highlightIndices = arrayfun(@(t) find(abs(timeVectorFocused - t) == min(abs(timeVectorFocused - t)), 1), highlightTimes);
highlightValues = averageStdDeviationFocused(highlightIndices);

% Plot the average standard deviation across latencies in the focused time range
figure;
plot(timeVectorFocused, averageStdDeviationFocused, 'LineWidth', 2);

% Add red circles to the peaks at 0 and 0.3125 seconds
hold on;
plot(highlightTimes, highlightValues, 'ro', 'MarkerSize', 10, 'LineWidth', 2);
hold off;

% Add a vertical line at 0 seconds
xline(0, '--k', 'LineWidth', 1.5); % Dashed black line at 0 seconds

% Customize the plot
xlabel('Time (seconds)');
ylabel('Global Field Power');
grid on;

% Ensure 0 seconds is included in the x-axis ticks
xticks(linspace(timeRange(1), timeRange(2), 11)); % Set ticks at the specified time range
xticks(sort([xticks, 0])); % Ensure 0 is included in the x-ticks

% Save the average figure in the plots folder
averagePlotFileName = fullfile(plotsFolder, 'Average_EEG_StdDeviation_from_-4_to_2_with_Highlighted_Peaks.png');
saveas(gcf, averagePlotFileName);

% Close the figure to avoid too many open figures
close(gcf);

disp('Plots for GFP from -4 to 2 seconds with highlighted peaks created and saved successfully.');
