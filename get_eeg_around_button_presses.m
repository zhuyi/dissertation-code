% Define the number of subjects
numSubjects = 10;

% Pre-allocate the structure to store the extracted segments for Fz and Pz
extractedData = struct('subjectData', cell(1, numSubjects));

% Sampling frequency and sample window parameters
fs = 64;
prePostSec = 5;
prePostSamples = prePostSec * fs;

% Define electrode indices for Fz and Pz
Fz_index = 38;
Pz_index = 31;

for subjectIndex = 1:10
    % Load the subject's data file
    fileName = fullfile(sprintf('pre_dataSub%d_with_button_presses.mat', subjectIndex));
    if exist(fileName, 'file')
        load(fileName, 'eeg'); % Assuming 'eeg' is the variable name in the .mat file
    else
        warning('File does not exist: %s', fileName);
        continue; % Skip to the next subject if the file doesn't exist
    end
    
    % Initialize a cell array for the current subject's data for Fz and Pz
    subjectElectrodeData = cell(2, 30);
    
    for trialIndex = 1:size(eeg.data, 2) % Iterate through each trial
        buttonPressData = eeg.data{2, trialIndex};
        buttonPressIndices = find(buttonPressData == 1);
        
        % Process for Fz and Pz electrodes
        for electrodeIdx = 1:2
            if electrodeIdx == 1
                currentElectrode = Fz_index;
            else
                currentElectrode = Pz_index;
            end
            
            % Initialize a matrix for storing segments for this electrode and trial
            electrodeSegments = [];
            
            for pressIdx = 1:length(buttonPressIndices)
                startIndex = max(1, buttonPressIndices(pressIdx) - prePostSamples);
                endIndex = min(size(eeg.data{1, trialIndex}, 1), buttonPressIndices(pressIdx) + prePostSamples);
                
                % Ensure we have a uniform length for each segment
                segmentLength = endIndex - startIndex + 1;
                if segmentLength == (prePostSamples * 2 + 1)
                    segment = eeg.data{1, trialIndex}(startIndex:endIndex, currentElectrode);
                    electrodeSegments = [electrodeSegments, segment];
                else
                    % Handle cases where the segment is near the start or end of the data
                    segment = zeros(prePostSamples * 2 + 1, 1); % Initialize with zeros
                    actualSegment = eeg.data{1, trialIndex}(startIndex:endIndex, currentElectrode);
                    padStart = prePostSamples - (buttonPressIndices(pressIdx) - startIndex);
                    segment((padStart+1):(padStart+length(actualSegment))) = actualSegment;
                    electrodeSegments = [electrodeSegments, segment];
                end
            end
            
            % Store the matrix of segments for this electrode and trial
            subjectElectrodeData{electrodeIdx, trialIndex} = electrodeSegments;
        end
    end
    
    % Store the data for the current subject in the structured array
    extractedData(subjectIndex).subjectData = subjectElectrodeData;
end

% Save the extracted data to a new .mat file
save('extracted_Fz_Pz_EEG_around_button_presses.mat', 'extractedData');

disp('EEG data around button presses for Fz and Pz extracted and saved successfully.');
