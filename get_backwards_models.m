function [eeg_tests,eeg_trains,modelAll,stim_tests,stim_trains] = get_backwards_models(stimIdx)
    addpath ./libs/cnsp_utils
    addpath ./libs/cnsp_utils/cnd
    addpath ./libs/mTRF-Toolbox_v2/mtrf
    addpath ./libs/NoiseTools
    addpath ./libs/eeglab
    
    dataMainFolder = '../Data/';
    dataCNDSubfolder = 'dataCND/';
    
    eegFilenames = dir([dataMainFolder,dataCNDSubfolder,'pre_dataSub*.mat']);
    stimFilenames = dir([dataMainFolder,dataCNDSubfolder,'pre_dataStim*.mat']);

    
    % TRF hyperparameters
    tmin = -150;
    tmax = 400;
    %lambdas = [1e-2,1e0,1e2]; % small set of lambdas (quick)
    %lambdas = [1e-6,1e-3,1e-4,1e-3,1e-2,1e-1,1e0,1,1e2,1e3,1e4]; % larger set of lambdas (slower)
    lambdas = logspace(-4,4,20); % Check across a large logarithmic space and
    %tune later
    %lambdas = linspace(1,11,11);
    dirTRF = -1; % Forward TRF model
    % Be careful: backward models (dirTRF-1) with many electrodes and large time-windows
    % can require long computational times. So, we suggest reducing the
    % dimensionality if you are just playing around with the code (e.g., select
    % only few electrodes and/or reduce the TRF window)
    
    %% TRF
    clear rAll rAllElec modelAll
    nSubs =10;
    
    stim_trains = {};
    stim_tests = {};
    eeg_trains = {};
    eeg_tests = {};
    for sub = 1:nSubs
        % Loading preprocessed EEG
        eegPreFilename = [dataMainFolder,dataCNDSubfolder,eegFilenames(sub).name];
        disp(['Loading preprocessed EEG data: ',eegFilenames(sub).name])
        load(eegPreFilename,'eeg')
        % Loading preprocessed stim
        stimPreFilename = [dataMainFolder,dataCNDSubfolder,stimFilenames(sub).name];
        disp(['Loading preprocessed stimulus data: ',stimFilenames(sub).name])
        load(stimPreFilename,'stim')
      
        
        % Selecting feature of interest ('stimIdx' feature)
        if(length(stimIdx) == 1)
            stimFeature = stim;
            stimFeature.names = stimFeature.names{stimIdx};
            stimFeature.data = stimFeature.data(stimIdx,:); % envelope or word onset
        else
            stimFeature = stim;
            stimFeature.names = stimFeature.names{stimIdx};
            stimFeature.data = stimFeature.data(stimIdx,:);
            newData = {};
            for i = 1:length(stimFeature.data)
                newData = [newData,do_horzcat(stimFeature.data(:,i),1:length(stimIdx))];
            end
            stimFeature.data = newData;
        end

        [stim_train,stim_test,eeg_train,eeg_test] = test_train_split(stimFeature,eeg,0.1);
        stim_trains = [stim_trains,stim_train];
        stim_tests = [stim_tests,stim_test];
        eeg_trains = [eeg_trains,eeg_train];
        eeg_tests = [eeg_tests,eeg_test];
        
        eeg = eeg_train;
        stimFeature = stim_train;
        
        % Making sure that stim and neural data have the same length
        % The trial may end a few seconds after the end of the audio
        % e.g., the neural data may include the break between trials
        % It would be best to do this chunking at preprocessing, but let's
        % check here, just to be sure
        [stimFeature,eeg] = cndCheckStimNeural(stimFeature,eeg);
        
        % Standardise stim data (preserving the ratio between features)
        % This is thought for continuous signals e.g., speech envelope, eeg
        stimFeature = cndNormalise(stimFeature);
        % Standardise neural data (preserving the ratio between channels)
        eeg = cndNormalise(eeg);
        
        % Ensuring that stim and ieeg have the same length
        [stimFeature,ieeg] = cndCutSameLength(stimFeature,eeg);
        
        % TRF crossvalidation - determining optimal regularisation parameter
        disp('Running mTRFcrossval')
        [stats,t] = mTRFcrossval(stimFeature.data,eeg.data,eeg.fs,dirTRF,tmin,tmax,lambdas,'verbose',0);
        
        % Calculating optimal lambda. Display and store results
        [maxR,bestLambda] = max(squeeze(mean(mean(stats.r,1),3)));
        disp(['r = ',num2str(maxR)])
        disp(['lambda = ',num2str(lambdas(bestLambda))])
        rAll(sub) = maxR;
        rAllElec(:,sub) = squeeze(mean(stats.r(:,bestLambda,:),1));
        
        % Fit TRF model with optimal regularisation parameter
        disp('Running mTRFtrain')
        model = mTRFtrain(stimFeature.data,eeg.data,eeg.fs,dirTRF,tmin,tmax,lambdas(bestLambda),'verbose',0);
        
        % Store TRF model
        modelAll(sub) = model;
        
        disp(['Mean r = ',num2str(mean(rAll))])
    end
    save("setup.mat","stim_trains","stim_tests","modelAll","eeg_trains","eeg_tests")
    clearvars -except eeg_tests eeg_trains modelAll stim_tests stim_trains
end

function result = do_horzcat(data,indices)
    result = data(1);
    for i = 2:length(indices)
        result = cellfun(@horzcat,result,data(i),'uni',0);
    end
end