function [stim_train,stim_test,resp_train,resp_test] = test_train_split(stim,resp,splitRatio)
% test_train_split: Makes a training and test split of trials for data in
% the CNSP format.
    stim_train = stim;
    stim_test = stim;
    resp_train = resp;
    resp_test = resp;

    stimData = stim.data;
    respData = resp.data;

    nTrials = length(stimData);
    shuffleOrder = randperm(nTrials);

    trainIndices = 1:floor(nTrials*(1-splitRatio));
    train = shuffleOrder(trainIndices);
    testIndices = floor(nTrials*(1-splitRatio))+1:nTrials;
    test = shuffleOrder(testIndices);

    stim_train.data = stim.data(:,train);
    stim_train.trial_nums = train;
    stim_test.data = stim.data(:,test);
    stim_test.trial_nums = test;

    resp_train.data = resp.data(train);
    resp_train.trial_nums = train;
    resp_test.data = resp.data(test);
    resp_test.trial_nums = test;
    for i = 1:length(resp.extChan)
        resp_train.extChan{i}.data = resp.extChan{i}.data(train);
        resp_test.extChan{i}.data = resp.extChan{i}.data(test);
    end
end