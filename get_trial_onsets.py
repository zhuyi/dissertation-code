import json
import os
from math import ceil

subject = 'Conn'

with open(f'../melody-expectation-experiment/stimuli/subjects/{subject}/blocks.json','r') as f:  
    trialsJSON = json.load(f)
    
with open('../Data/MIDI/onsets/onsets.json','r') as f:
    onsets = json.load(f)
    
expOnsets = {}

for i,key in enumerate(trialsJSON):
    block = trialsJSON[key]
    for j,trialKey in enumerate(block):
        if(trialKey != 'instrument'):
            trialOnsets = []
            trial = block[trialKey]
            prevEnd = 0
            for segment in trial:
                seg = trial[segment]
                name = seg['song_name'].split('.')[0] + '.mid'
                start = seg['segment_start']/1000
                length = seg['segment_length']/1000
                end = start + length
                
                segOnsets = [x for x in onsets[name] if start <= x <= end]
                m = min(segOnsets)
                segOnsets = list(map(lambda x : x-m,segOnsets))
                    
                    
                trialOnsets.extend(list(map(lambda x: x+prevEnd,segOnsets)))
                prevEnd += length
                print(prevEnd,length)
                
            expOnsets[f'trial_{(i*5)+j}'] = trialOnsets
   
os.makedirs('../Data/onsets/',exist_ok=True)
with open(f'../Data/onsets/{subject}_note_onsets.json','w+') as f:
    json.dump(expOnsets,f)
                