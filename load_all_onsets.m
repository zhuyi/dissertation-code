function [onsets,constructedEnvelopes] = load_all_onsets(onsets_path,fs,lengths)
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    str = fileread(onsets_path); 
    val = jsondecode(str);

    str = fileread("../Data/MIDI/onsets/suprisals.json");
    sval = jsondecode(str);

    names = fieldnames(val);

    onsets = {};
    constructedEnvelopes ={};

    for i = 1:length(names)
        len = lengths{i};
        field = val.(names{i}).onsets;
        field = int32(round(field*fs))+1;
        
        onset = zeros([1,len]);
        constructedEnvelope = zeros([1,len]);
        onset(field) = 1;
        onsets = [onsets,transpose(onset)];
        for j = 1:length(field)
            index = field(j);
            %disp(index)
            if(index>len)
                break;
            end
            if(names{i} == "Thriller_mid")
                continue;
            end
            sname = strsplit(names{i},'_');
            surprise = sval.(sname{1})(i);
            constructedEnvelope(index) = constructedEnvelope(index) + (1*surprise);
            for k = 1:5
                if(index + k< len)
                    constructedEnvelope(index + k) = constructedEnvelope(index+k) + ((1/(k+1))*surprise);
                end
            end
        end
        outD = {};
        outD.data = transpose(constructedEnvelope);
        outD.name = names{i};
        outD.instrument = "Piano";
        constructedEnvelopes = [constructedEnvelopes,outD];
    end
end

