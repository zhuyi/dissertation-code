function corrs = sliding_window_correlations(eegSection,wav,model,windowStep)
    corrs = [];
    
    yLen = length(wav);
    windowLen = length(eegSection)-1;

    for i = 0:floor((yLen-windowLen)/windowStep)-1
        windowWav = wav((i*windowStep)+1:((i*windowStep)+1)+windowLen,:);
        
        %windowYPred = normalize(abs(hilbert((mTRFpredict([],eegSection,model,'verbose',0)))));
        corr = corrcoef(windowWav,eegSection);
        corrs = [corrs corr(2,1)];
    end

end