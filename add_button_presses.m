% Define the root directories for the data
dataDir = fullfile('..', 'Data', 'dataCND');
behaviouralDir = fullfile('..', 'melody-expectation-experiment', 'behavioural');
outputDir = fullfile('..', 'Code');

% Sampling frequency
fs = 64;

for subjectIndex = 1:10
    % Load the pre_dataStim.mat file for the current subject
    stimFilePath = fullfile(dataDir, ['pre_dataStim', num2str(subjectIndex), '.mat']);
    if exist(stimFilePath, 'file')
        load(stimFilePath); % Loads the 'stim' structure
    else
        disp(['File does not exist: ', stimFilePath]);
        continue; % Skip this subject if the file doesn't exist
    end

    % Load the pre_dataSub.mat file for the current subject
    subFilePath = fullfile(dataDir, ['pre_dataSub', num2str(subjectIndex), '.mat']);
    if exist(subFilePath, 'file')
        load(subFilePath); % Loads the 'sub' structure
    else
        disp(['File does not exist: ', subFilePath]);
        continue; % Skip this subject if the file doesn't exist
    end

    % Load the buttonPresses.json file for the current subject
    jsonFilePath = fullfile(behaviouralDir, ['subject_', num2str(subjectIndex)], 'buttonPresses.json');
    if exist(jsonFilePath, 'file')
        jsonText = fileread(jsonFilePath);
        buttonPressesData = jsondecode(jsonText);
    else
        disp(['File does not exist: ', jsonFilePath]);
        continue; % Skip this subject if the file doesn't exist
    end

    % Process each block and trial contained in the buttonPresses.json
    blockNames = fieldnames(buttonPressesData);
    for blockIndex = 1:length(blockNames)
        blockName = blockNames{blockIndex};
        currentBlock = buttonPressesData.(blockName);

        trialNames = fieldnames(currentBlock);
        for trialIndex = 1:length(trialNames)
            trialName = trialNames{trialIndex};
            buttonPressTimes = currentBlock.(trialName);
            
            % Convert button press times to sampling points
            buttonPressSamples = round(buttonPressTimes * fs);
            
            % Determine the total number of samples for this trial
            totalSamples = size(stim.data{1, (blockIndex - 1) * 5 + trialIndex}, 1);
            
            % Initialize buttonPressData with zeros
            buttonPressData = zeros(totalSamples, 1);
            
            % Mark button press points in buttonPressData
            for i = 1:length(buttonPressSamples)
                if buttonPressSamples(i) <= totalSamples
                    buttonPressData(buttonPressSamples(i)) = 1;
                end
            end

            % Insert buttonPressData into the appropriate cell in stim.data
            stim.data{6, (blockIndex - 1) * 5 + trialIndex} = buttonPressData;
            % Insert buttonPressData into the appropriate cell in sub.data
            eeg.data{2, (blockIndex - 1) * 5 + trialIndex} = buttonPressData;
        end
    end

    % Save the updated stim structure back to a new file specific to the subject
    save(fullfile(outputDir, ['pre_dataStim', num2str(subjectIndex), '_with_button_presses.mat']), 'stim');
    % Save the updated sub structure back to a new file specific to the subject
    save(fullfile(outputDir, ['pre_dataSub', num2str(subjectIndex), '_with_button_presses.mat']), 'eeg');
end

disp('Button presses added to new files successfully.');
