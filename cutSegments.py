from pydub import AudioSegment
import json

subject = 'Conn'

def cutSegments(subjectBlocksPath,subNum,subjectTrialWavsPath,outputPath,badSongs):
    with open(subjectBlocksPath,'r') as f:  
        trialsJSON = json.load(f)
        
    for i,blockKey in enumerate(trialsJSON):
        print(blockKey)
        for j,trialKey in enumerate(trialsJSON[blockKey]):
            
            if(trialKey == 'instrument'):continue
            wav = AudioSegment.from_wav(f'{subjectTrialWavsPath}/subject_{subNum}_trial_{(i*5)+j}.wav')
            trial = trialsJSON[blockKey][trialKey]
            contructedwav = AudioSegment.empty()
            
            prevEndTime = 0
            trialOnset = []
            #print(trialKey)
            for segKey in trial:
                segment = trial[segKey]
                if segment['song_name'].split('.')[0] not in badSongs:
                    start = segment['segment_start'] + prevEndTime
                    length = segment['segment_length']
                    end = start + length
                    #print(len(wav[prevEndTime:prevEndTime+length]))
                    contructedwav += wav[prevEndTime:prevEndTime+length]
                    prevEndTime += length
                else:
                    print('bad')
                    prevEndTime += segment['segment_length']
                    
            print(len(contructedwav))  
            contructedwav.export(f'{outputPath}/subject_{subNum}_trial_{(i*5)+j}.wav',format='wav')
                
                
            