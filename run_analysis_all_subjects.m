names = {"Subject_1","Subject_10","Subject_2","Subject_3","Subject_4","Subject_5","Subject_6","Subject_7","Subject_8","Subject_9"};

eeg_step = 2;
eeg_length = 4;

allTests = {};

for i = 1:length(names)
    tests = run_analysis(i,eeg_step,eeg_length,names{i});
    allTests{i} = tests;
end