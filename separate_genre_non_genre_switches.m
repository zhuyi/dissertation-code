% Separate genre switches from non-genre switches
numSubjects = 10;
fs = 64; % Sampling frequency
prePostSec = 5;
prePostSamples = prePostSec * fs;

% Initialize structures to store genre and non-genre switch indices
genreSwitches = struct('subjectData', cell(1, numSubjects));
nonGenreSwitches = struct('subjectData', cell(1, numSubjects));

for subjectIndex = 1:numSubjects
    % Define path to the blocks.json file for the current subject
    jsonFilePath = fullfile('..', 'melody-expectation-experiment', 'stimuli', 'subjects', ['subject_', num2str(subjectIndex)], 'blocks.json');
    
    % Check if the blocks.json file exists
    if ~exist(jsonFilePath, 'file')
        warning('blocks.json file not found for subject %d', subjectIndex);
        continue;
    end
    
    % Load JSON data
    jsonText = fileread(jsonFilePath);
    jsonData = jsondecode(jsonText);

    % Initialize cell arrays to store switch indices for each trial
    genreSwitchIndices = cell(1, 30);
    nonGenreSwitchIndices = cell(1, 30);

    % Process each block and trial contained in the blocks.json
    blockNames = fieldnames(jsonData);
    for blockIdx = 1:length(blockNames)
        blockName = blockNames{blockIdx};
        currentBlock = jsonData.(blockName);

        trialNames = fieldnames(currentBlock);
        for trialIdx = 1:length(trialNames)
            trialName = trialNames{trialIdx};
            trialData = currentBlock.(trialName);

            % Initialize variables to track previous genre
            previousGenre = '';

            % Dynamic handling of segments since they're not in a 'segments' array
            segmentNames = fieldnames(trialData);
            switchIndices = [];
            for segIdx = 1:numel(segmentNames)
                segment = trialData.(segmentNames{segIdx});
                
                % Check if genre has changed
                if ~isempty(previousGenre) && ~strcmp(segment.genre, previousGenre)
                    switchIndices = [switchIndices, segIdx];
                end
                previousGenre = segment.genre;
            end
            
            % Separate genre and non-genre switches
            genreSwitchIndices{trialIdx} = [];
            nonGenreSwitchIndices{trialIdx} = [];
            for i = 1:length(switchIndices)
                segIdx = switchIndices(i);
                currentGenre = trialData.(segmentNames{segIdx}).genre;
                previousGenre = trialData.(segmentNames{segIdx-1}).genre;
                if strcmp(currentGenre, previousGenre)
                    nonGenreSwitchIndices{trialIdx} = [nonGenreSwitchIndices{trialIdx}, segIdx];
                else
                    genreSwitchIndices{trialIdx} = [genreSwitchIndices{trialIdx}, segIdx];
                end
            end
        end
    end
    
    % Store the switch indices for the current subject
    genreSwitches(subjectIndex).subjectData = genreSwitchIndices;
    nonGenreSwitches(subjectIndex).subjectData = nonGenreSwitchIndices;
end

% Save the switch indices to new .mat files
save('genre_switches.mat', 'genreSwitches');
save('non_genre_switches.mat', 'nonGenreSwitches');

disp('Genre and non-genre switch indices saved successfully.');
