% Ensure EEGLAB is installed and added to your MATLAB path
if ~exist('eeglab', 'file')
    error('EEGLAB is not installed or not added to the MATLAB path.');
end

% Load EEGLAB
[ALLEEG, EEG, CURRENTSET, ALLCOM] = eeglab;

% Load the averaged EEG data
load('averaged_EEG_around_switches.mat', 'averagedData');

% Number of subjects and electrodes
numSubjects = length(averagedData);
numElectrodes = 64;
numLatencies = 641; % prePostSamples * 2 + 1
fs = 64; % Sampling frequency
timeVector = linspace(-5, 5, numLatencies); % Time vector from -5 to +5 seconds

% Time points of interest (in seconds)
timePoints = [0.3281, 0.6406, 1.3125];

% Find the indices of the specified time points
timeIndices = arrayfun(@(t) find(abs(timeVector - t) == min(abs(timeVector - t)), 1), timePoints);

% Create a folder for the plots if it doesn't exist
plotsFolder = 'EEG_Topography_Plots';
if ~exist(plotsFolder, 'dir')
    mkdir(plotsFolder);
end

% Load channel locations from chanlocs64.mat
load('chanlocs64.mat', 'chanlocs');

% Ensure the channel locations match the number of electrodes
if length(chanlocs) ~= numElectrodes
    error('The number of channels in the location file does not match the number of electrodes in the data.');
end

% Loop through each time point and create the topography plots
for i = 1:length(timePoints)
    % Initialize an array to store the average values across subjects for each electrode
    avgValues = zeros(numElectrodes, 1);
    
    % Loop through each subject and accumulate the data for the current time point
    for subjectIndex = 1:10
        subjectData = averagedData(subjectIndex).subjectData;
        avgValues = avgValues + subjectData(:, timeIndices(i));
    end
    
    % Compute the average values across subjects
    avgValues = avgValues / numSubjects;
    
    % Create the topographic plot
    figure;
    topoplot(avgValues, chanlocs, 'maplimits', 'absmax', 'electrodes', 'on');
    title(sprintf('Topography at %.4f seconds', timePoints(i)));
    
    % Save the figure in the plots folder
    plotFileName = fullfile(plotsFolder, sprintf('Topography_%.4f_seconds.png', timePoints(i)));
    saveas(gcf, plotFileName);
    
    % Close the figure to avoid too many open figures
    close(gcf);
end

disp('Topographic plots created and saved successfully.');
