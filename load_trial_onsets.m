function [onsets,altEnvelopes] = load_trial_onsets(filename,lengths,fs)
    str = fileread(filename); 
    val = jsondecode(str);

    names = fieldnames(val);

    onsets = {};
    altEnvelopes ={};

    for i = 1:length(names)
        len = lengths{i};
        field = val.(names{i}).onsets;
        field = int32(round(field*fs))+1;
        
        onset = zeros([1,len]);
        altEnvelope = zeros([1,len]);
        onset(field) = 1;
        onsets = [onsets,transpose(onset)];
        disp(i)
        for j = 1:length(field)
            index = field(j);
            if(index>len)
                break;
            end
            if(altEnvelope(index) ~= 0)
                continue;
            end
            surprise = val.(names{i}).surprisals(j);
            if(surprise>=1)
                disp(j)
                disp(surprise)
            end
            altEnvelope(index) = altEnvelope(index) + (1*surprise);
            for k = 1:5
                if(index + k< len)
                    altEnvelope(index + k) = altEnvelope(index+k) + ((1/k)*surprise);
                end
            end
        end
        altEnvelopes = [altEnvelopes,transpose(altEnvelope)];
    end

end