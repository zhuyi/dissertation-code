function ranks = get_most_correlated_wav(corrs,wavs,k)

    for i =1:length(corrs)
        corr = corrs{i};
        sortedCorr = sort(corr,'descend');
        topCorr = sortedCorr(1:ceil(end/10));
        maxCorr = max(corr);
        
        rank.score = maxCorr;%mean(topCorr);
        rank.name = wavs{i}.name;
        rank.instrument = wavs{i}.instrument;
        ranks(i) = rank;
    end

    scores = [ranks(:).score];
    ranks = ranks(ismember(scores,intersect(scores,maxk(scores,k))));
    [~,idx] = sort([ranks.score],'descend');
    ranks = ranks(idx);
end

