import os
import random
import json
from pydub import AudioSegment


genreNames = ['Pop','Bach','Jazz','DaftPunk']

# Lists dirs in a MacOS safe way
def listdir(path):
    dir = os.listdir(path)
    try:
        dir.remove(".DS_Store")
    except:
        pass
    return dir

# Gets subdirs in a MacOS safe way
def getFolderStructure(base_path):
    structures = {}
    for name in genreNames:
        structures[name] = listdir(f'{base_path}/{name}')
    return structures

# Gets a random segment of random lenght from a given song
def getSegments(songPath):
    # The range that the segment lengths are taken from
    maxSegmentDuration = 23
    minSegmentDuration = 17
    newAudio = AudioSegment.from_file(songPath, "wav")

    audioLength = int(len(newAudio) / 1000)
    chosenLength = random.randint(minSegmentDuration, maxSegmentDuration)
    startPoint = random.randint(0, audioLength - chosenLength) * 1000
    chosenLength = chosenLength * 1000

    return newAudio[startPoint : startPoint + chosenLength], startPoint,chosenLength

# Creates a single trial
def create_trial(base_path,STIMS_PER_TRIAL):
    # Choose the genres in the trial
    selectedGenres = []

    for i in range(int(STIMS_PER_TRIAL/len(genreNames))):
        # Ensure that there's a difference between the blocks
            
        addedGenres = random.choices(genreNames,k=len(genreNames))
        if selectedGenres == []:
            selectedGenres+= addedGenres
            continue
        # while addedGenres[0] == selectedGenres[-1]:
        #     addedGenres = random.sample(genreNames,len(genreNames))
        selectedGenres += addedGenres
        

    if STIMS_PER_TRIAL%len(genreNames) != 0:
        addedGenres = random.sample(genreNames,STIMS_PER_TRIAL%len(genreNames))
        # while addedGenres[0] == selectedGenres[-1]:
        #     addedGenres = random.sample(genreNames,STIMS_PER_TRIAL%len(genreNames))
        selectedGenres += addedGenres\
        
    selectedGenres = random.choices(genreNames,k=STIMS_PER_TRIAL)
    structures = getFolderStructure(base_path)
    constructedSong = AudioSegment.empty()

    trialInfo = {}

    for i, genre in enumerate(selectedGenres):
        chosenSong = random.sample(structures[genre],1)[0]
        segment,start,length = getSegments(os.path.join(base_path,genre,chosenSong))

        constructedSong += segment

        segmentInfo = {}
        segmentInfo['genre'] = genre
        segmentInfo['song_name'] = chosenSong
        segmentInfo['segment_start'] = start
        segmentInfo['segment_length'] = length

        trialInfo[f'segement_{i}'] = segmentInfo

    print(selectedGenres)
    return constructedSong,trialInfo

# Creates a single block
def create_block(base_path,output_path,block_number,TRIALS_PER_BLOCK=5,STIMS_PER_TRIAL=6):
    blockInfo = {}

    for i in range(TRIALS_PER_BLOCK):
        newSong,newInfo = create_trial(base_path,STIMS_PER_TRIAL)
        out_path = os.path.join(output_path,f'trial_{i}')

        if not os.path.exists(out_path):
            os.makedirs(out_path)

        newSong.export(os.path.join(output_path,f'trial_{i}',f'trial_{i}.wav'),format='wav')

        with open(os.path.join(out_path,f'trial_{i}.json'),'w+') as outfile:
            json.dump(newInfo,outfile,indent=4)

        blockInfo[f'trial_{i}'] = newInfo

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    # with open(os.path.join(output_path, f'block_{str(block_number)}.json'),'w+') as outfile:
    #         json.dump(blockInfo, outfile, indent=4)

    return blockInfo
    

    

if __name__ == '__main__':
    base_path = './melody-expectation-experiment/stimuli/components/Piano'
    output_path = './melody-expectation-experiment/stimuli/subjects/test'
    create_block(base_path,output_path,0)
