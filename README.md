This repository contains the code used and some graphical results from the study *Investigating Neural Signatures of Genre Detection
during Melody Listening*.

After the data was recorded from EEG experiments and stored in the computer, it went through the following process to be analyzed:

preprocess.py -> convert_to_CND.m -> CNSP2022_example1_forwardTRF.m -> get_backwards_models.m -> get_backwards_models.m -> run_analysis_all_subjects.m -> name_to_genre_dictionary.m -> genre_accuracy_baseline_all_subjects.m -> genre_accuracy_all_subjects.m -> compare_model_with_baseline.m -> add_switches.m -> get_eeg_around_switches.m -> create_electrode_x_latency_matrix.m -> calculate_standard_deviation_vector.m -> draw_standard_deviation_each_subject_for_switches.m -> draw_averaged_standard_deviation_with_peaks_for_switches.m -> draw_topoplot_switches.m -> add_button_presses.m -> get_eeg_around_button_presses.m -> create_electrode_x_latency_matrix_for_button_presses.m -> calculate_standard_deviation_vector_for_button_presses.m -> draw_standard_deviation_each_subject_for_button_presses.m -> draw_averaged_standard_deviation_with_peaks_for_button_presses.m -> draw_topoplot_button_presses.m -> draw_from_minus_four_to_two_button_presses.m -> draw_eeg_around_button_presses_two_electrodes.m.



A brief summary of what each file does:

**preprocess.py**: Preprocess the stimuli data.

**convert_to_CND.m**: Convert the stimuli data and the EEG data to CND format.

**CNSP2022_example1_forwardTRF.m**: Preprocess the EEG data.

**get_backwards_models.m**: Fit the backward TRF model.

**run_analysis_all_subjects.m**: Reconstruct and correlate stimuli.

**name_to_genre_dictionary.m**: Map each stimulus to a genre.

**genre_accuracy_baseline_all_subjects.m**: Calculate genre accuracies of the baseline model.

**genre_accuracy_all_subjects.m**: Calculate genre accuracies of the genre classifier for each subject.

**compare_model_with_baseline.m**: Compare the genre accuracies of the genre classifer and the baseline model and draw the comparison graph.

**add_switches.m**: Add time points of the switches to the stimuli data and the EEG data.

**get_eeg_around_switches.m**: Extract the EEG data around the switches.

**create_electrode_x_latency_matrix.m**: Average the EEG data across trials and switches for each subject.

**calculate_standard_deviation_vector.m**: Calculate GFP around switches.

**draw_standard_deviation_each_subject_for_switches.m**: Draw GFP around switches for each subject.

**draw_averaged_standard_deviation_with_peaks_for_switches.m**: Draw GFP around switches averaged across subjects.

**draw_topoplot_switches.m**: Draw topographies at peaks around switches.

**add_button_presses.m**: Add time points of the button presses to the stimuli data and the EEG data.

**get_eeg_around_button_presses.m**: Extract the EEG data around the button presses.

**create_electrode_x_latency_matrix_for_button_presses.m**: Average the EEG data across trials and button presses for each subject.

**calculate_standard_deviation_vector_for_button_presses.m**: Calculate GFP around button presses.

**draw_standard_deviation_each_subject_for_button_presses.m**: Draw GFP around button presses for each subject.

**draw_averaged_standard_deviation_with_peaks_for_button_presses.m**: Draw GFP around button presses averaged across subjects.

**draw_topoplot_button_presses.m**: Draw topographies at peaks around button presses.

**draw_from_minus_four_to_two_button_presses.m**: Draw GFP around button presses focusing on evidence accumulation.

**draw_eeg_around_button_presses_two_electrodes.m**: Draw ERP around button presses for two specific electrodes.