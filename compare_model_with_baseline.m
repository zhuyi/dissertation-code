allBaselineAccuracies = [totalAccuracies{:}];

% Start timing
tic

% Convert cell array to double array for plotting
accuraciesDouble = cell2mat(accuracies);

% Create figure
fig = figure;

% Plot histogram of baseline accuracies
hist = histogram(allBaselineAccuracies, 37, "Normalization", "pdf", "FaceColor", "white", 'DisplayName', "Baseline Model Distribution");
hold on;

% Calculate mean and standard deviation of baseline accuracies
mu = mean(allBaselineAccuracies);
sigma = std(allBaselineAccuracies);

% Generate PDF values for plotting
y = min(allBaselineAccuracies):0.001:max(allBaselineAccuracies);
f = exp(-(y - mu).^2 ./ (2 * sigma^2)) ./ (sigma * sqrt(2 * pi));

% Plot the ideal normal distribution
dist = plot(y, f, "LineWidth", 1.5, "Color", [0 0.4470 0.7410], "DisplayName", "Ideal distribution");

% Calculate 95th percentile of baseline accuracies
P95 = prctile(allBaselineAccuracies, 95);

% Plot accuracies of the model
subLines = xline(accuraciesDouble, '--r', {'Subject 1', 'Subject 10', 'Subject 2', 'Subject 3', 'Subject 4', 'Subject 5', 'Subject 6', 'Subject 7', 'Subject 8', 'Subject 9'}, "DisplayName", "Model Accuracies");
set(subLines, 'LabelVerticalAlignment', 'top', 'FontSize', 24);

% Plot the 95th percentile line
line95 = xline(P95, '-k', {'95th Percentile of Distribution'}, 'LineWidth', 1.5, "DisplayName", "Distribution 95th percentile");
line95.LabelVerticalAlignment = "middle";
line95.FontSize = 24;

% Set title and labels
title("PDF of Baseline Model vs Accuracies of Model")
xlabel("Accuracy (%)");
ylabel("PDF value");

% Create legend
legend([dist subLines(1) line95], {'Ideal distribution', 'Model Accuracies', 'Distribution 95th percentile'}, "Location", "northwest");

% Set font size of axes
ax = gca;
ax.FontSize = 24;

% Stop holding the plot
hold off;

% Stop timing
toc
