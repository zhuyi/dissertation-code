function [expected,expected_genre,instrument,overlap] = get_expected_label(subject,trialNum,start,fin)
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    expected = {};
    str = fileread(['../melody-expectation-experiment/stimuli/subjects/',char(subject),'/good_blocks.json']); 
    val = jsondecode(str);

    blockNum = floor((trialNum-1)/5);
    trialInBlockNum = floor(mod((trialNum-1),5));

    segs = val.(['block_',int2str(blockNum)]).(['trial_',int2str(trialInBlockNum)]);
    instrument = val.(['block_',int2str(blockNum)]).instrument;
    overlap = false;

    segLength = fin-start;
    currentLength = 0;
    for i = 1:length(fieldnames(segs))
        currentEnd = currentLength + (segs.(['segement_',int2str(i-1)]).segment_length/1000);
        if(currentEnd-start >= segLength/2)
            expected = segs.(['segement_',int2str(i-1)]).song_name;
            expected_genre = segs.(['segement_',int2str(i-1)]).genre;
            if (fin - currentEnd > 0 || start - currentLength < 0)
                overlap = true;
            end
            break;
        end
        currentLength = currentLength + (segs.(['segement_',int2str(i-1)]).segment_length/1000);
    end
end

