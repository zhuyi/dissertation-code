% Define the root directories for the data
dataDir = fullfile('..', 'Data', 'dataCND');
stimuliDir = fullfile('..', 'melody-expectation-experiment', 'stimuli', 'subjects');
outputDir = fullfile('..', 'Code');

% Number of subjects
numSubjects = 10;
numElectrodes = 64;
fs = 64;
prePostSec = 5;
prePostSamples = prePostSec * fs;

% Initialize structures to store extracted EEG data
extractedGenreData = struct('subjectData', cell(1, numSubjects));
extractedNonGenreData = struct('subjectData', cell(1, numSubjects));

% Loop through each subject
for subjectIndex = 1:numSubjects
    % Load the pre_dataSub.mat file for the current subject
    subFilePath = fullfile(dataDir, ['pre_dataSub', num2str(subjectIndex), '.mat']);
    if exist(subFilePath, 'file')
        load(subFilePath); % Loads the 'eeg' structure
    else
        disp(['File does not exist: ', subFilePath]);
        continue; % Skip this subject if the file doesn't exist
    end

    % Load the blocks.json file for the current subject
    jsonFilePath = fullfile(stimuliDir, ['subject_', num2str(subjectIndex)], 'blocks.json');
    if exist(jsonFilePath, 'file')
        jsonText = fileread(jsonFilePath);
        blocksData = jsondecode(jsonText);
    else
        disp(['File does not exist: ', jsonFilePath]);
        continue; % Skip this subject if the file doesn't exist
    end

    % Initialize cell arrays to store EEG data for genre and non-genre switches
    subjectGenreData = cell(numElectrodes, 30); % Assuming 30 trials
    subjectNonGenreData = cell(numElectrodes, 30); % Assuming 30 trials

    % Loop through each block and trial
    blockNames = fieldnames(blocksData);
    for blockIdx = 1:length(blockNames)
        block = blocksData.(blockNames{blockIdx});
        trialNames = fieldnames(block);
        for trialIdx = 1:length(trialNames)
            trial = block.(trialNames{trialIdx});
            switchIndices = [];
            genreSwitches = [];
            segmentNames = fieldnames(trial);

            % Identify switch indices and types
            for segIdx = 1:length(segmentNames) - 1
                currentSegment = trial.(segmentNames{segIdx});
                nextSegment = trial.(segmentNames{segIdx + 1});
                switchIndices = [switchIndices, currentSegment.segment_start + currentSegment.segment_length];
                if strcmp(currentSegment.genre, nextSegment.genre)
                    genreSwitches = [genreSwitches, false];
                else
                    genreSwitches = [genreSwitches, true];
                end
            end
            
            switchIndices = round(switchIndices / 1000 * fs); % Convert to samples

            % Extract EEG data around genre and non-genre switches
            for electrodeIdx = 1:numElectrodes
                genreSegments = [];
                nonGenreSegments = [];

                for switchIdx = 1:length(switchIndices)
                    startIndex = max(1, switchIndices(switchIdx) - prePostSamples);
                    endIndex = min(size(eeg.data{1, (blockIdx - 1) * 5 + trialIdx}, 1), switchIndices(switchIdx) + prePostSamples);
                    segmentLength = endIndex - startIndex + 1;

                    if segmentLength == (prePostSamples * 2 + 1)
                        segment = eeg.data{1, (blockIdx - 1) * 5 + trialIdx}(startIndex:endIndex, electrodeIdx);
                        if genreSwitches(switchIdx)
                            genreSegments = [genreSegments, segment];
                        else
                            nonGenreSegments = [nonGenreSegments, segment];
                        end
                    else
                        % Pad or trim the segment if it doesn't meet the expected length
                        segment = zeros(prePostSamples * 2 + 1, 1);
                        actualSegment = eeg.data{1, (blockIdx - 1) * 5 + trialIdx}(startIndex:endIndex, electrodeIdx);
                        padStart = prePostSamples - (switchIndices(switchIdx) - startIndex);
                        segment((padStart + 1):(padStart + length(actualSegment))) = actualSegment;
                        if genreSwitches(switchIdx)
                            genreSegments = [genreSegments, segment];
                        else
                            nonGenreSegments = [nonGenreSegments, segment];
                        end
                    end
                end

                subjectGenreData{electrodeIdx, (blockIdx - 1) * 5 + trialIdx} = genreSegments;
                subjectNonGenreData{electrodeIdx, (blockIdx - 1) * 5 + trialIdx} = nonGenreSegments;
            end
        end
    end

    extractedGenreData(subjectIndex).subjectData = subjectGenreData;
    extractedNonGenreData(subjectIndex).subjectData = subjectNonGenreData;
end

% Save the extracted data to new .mat files
save(fullfile(outputDir, 'extracted_EEG_around_genre_switches.mat'), 'extractedGenreData');
save(fullfile(outputDir, 'extracted_EEG_around_non_genre_switches.mat'), 'extractedNonGenreData');

disp('Extracted EEG data for genre and non-genre switches saved successfully.');
