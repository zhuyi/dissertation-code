from fix_blocks import fix_blocks
from get_trial_onsets_1 import get_trial_onsets
from cutSegments import cutSegments

subjects = ['subject_1','subject_2','subject_3','subject_4','subject_5','subject_6','subject_7','subject_8','subject_9','subject_10']
badSongs = [['Thriller','HotelCalifornia'],['Thriller'],[],[],[],[],[],[],[],[]]
#subjects = ['Conn']
#badSongs = [['Thriller','HotelCalifornia']]
subs = zip(subjects,badSongs)

for i,sub in enumerate(subs):
    print(sub)
    subName = sub[0]
    badSongs = sub[1]
    subjectBlocksJson = f'../melody-expectation-experiment/stimuli/subjects/{subName}'

    print('Fixing blocks')
    fix_blocks(subjectBlocksJson,badSongs)

    subjectBlocksJson = subjectBlocksJson + '/good_blocks.json'
    subTrialWavs = f'../Data/stim/subject_{i+1}'
    outputPath = f'../Data/stim/subject_{i+1}/bad_removed'
    print(subjectBlocksJson)

    print('Cutting segments')
    cutSegments(subjectBlocksJson,i+1,subTrialWavs,outputPath,badSongs)

    outputPath = f'../Data/onsets/subject_{i+1}_note_onsets.json'
    
    print('Getting trial onsets')
    get_trial_onsets(subjectBlocksJson,'../Data/MIDI/onsets/onsets.json',outputPath)