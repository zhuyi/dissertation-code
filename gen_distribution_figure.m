tic
fig = figure;
hist = histogram(dummy,37,"Normalization","pdf","FaceColor","white",'DisplayName',"");
hold on;

mu = 0.25;
sigma = std(dummy);

y = 0.1:0.001:0.4;
f = exp(-(y-mu).^2./(2*sigma^2))./(sigma*sqrt(2*pi));

dist = plot(y,f,"LineWidth",1.5,"Color",[0 0.4470 0.7410],"DisplayName","Ideal distribution");
P95 = prctile(dummy, 95);

subLines = xline(accuracies,'--r',{'Subject 1','Subject 2','Subject 3','Subject 4'},"DisplayName","Subject top 5 accuracy");
subLine = xline(accuracies(1),'--r',{'Subject 1'});
line95 = xline(P95,'-k',{'95th Percentile of Distribution'},'LineWidth',1.5,"DisplayName","Distribution 95th percentile");
line95.LabelVerticalAlignment = "middle";

title("PDF of Dummy model vs top 5 accuracies of Subjects")
xlabel("Top 5 accuracy (%)");
ylabel("PDF value");

legend([dist subLine line95],{'Ideal distribution','Subject top 5 accuracy','Distribution 95th percentile'},"Location","northwest");
ax = gca;
ax.FontSize = 16;
hold off;
toc