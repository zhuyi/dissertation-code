import json
import os

subject = 'Conn'

def get_trial_onsets(subjectJSONPath,midiOnsetsPath,onsetsOutPath):
    with open(subjectJSONPath,'r') as f:  
        trialsJSON = json.load(f)
        
    with open(midiOnsetsPath,'r') as f:
        onsets = json.load(f)
        
        
    expOnsets = {}
    overlapCount = 0

    #trial = trialsJSON['block_0']['trial_0']
    for i,blockKey in enumerate(trialsJSON):
        print(blockKey)
        for j,trialKey in enumerate(trialsJSON[blockKey]):
            if(trialKey == 'instrument'):continue
            trial = trialsJSON[blockKey][trialKey]
            
            prevEndTime = 0
            trialOnset = []
            trialPitches = []
            trialSurprisals = []
            print(trialKey)
            for segKey in trial:
                segment = trial[segKey]
                print(segment)
                name = segment['song_name'].split('.')[0] + '.mid'
                start = segment['segment_start']/1000
                length = segment['segment_length']/1000
                if length == 0: 
                    print('bad')
                    continue
                end = start + length
                
                onsetsS = 'onsets'
                ends = 'ends'
            
                segOnsets = list([x for x in onsets[name]['onsets'] if start <= x  and x <= end])
                if(len(segOnsets)<=0):
                    print('bad Onsets')
                    startIndex = 0
                    endIndex = 0
                else:
                    startIndex = onsets[name]['onsets'].index(segOnsets[0])
                    endIndex = onsets[name]['onsets'].index(segOnsets[-1])
                
                segPitches = list([v for i,v in enumerate(onsets[name]['pitches']) if startIndex <= i  and i <= endIndex])
                segSurprisals = list([v for i,v in enumerate(onsets[name]['surprisals']) if startIndex <= i  and i <= endIndex])
                if(len(segOnsets)!=len(segSurprisals)):
                    print(len(segOnsets),len(segSurprisals))
                priorNoteIndex = 0
                if((start != 0) and (min(onsets[name]['onsets'])<=start)):
                    priorNoteIndex = max([i for i,x in enumerate(onsets[name]['onsets']) if x < start])
                diff = onsets[name][ends][priorNoteIndex]-start
                if (onsets[name]['ends'][priorNoteIndex]>start and onsets[name]['ends'][priorNoteIndex]<segOnsets[0] and priorNoteIndex != 0 and diff > 1/64):
                    print(f'Overlap')
                    overlapCount+=1
                    
                    segOnsets = [start] + (segOnsets)
                    segPitches = [start] + (segPitches)
                    segSurprisals = [start] + (segSurprisals)
                    # print(type(segOnsets))
                    
                segOnsets = list(map(lambda x:x-start+prevEndTime,segOnsets))
                trialOnset.extend(segOnsets)
                trialPitches.extend(segPitches)
                trialSurprisals.extend(segSurprisals)
                prevEndTime += length
                
            expOnsets[f'trial_{(i*5)+j}'] = {}
            expOnsets[f'trial_{(i*5)+j}']['onsets'] = trialOnset
            expOnsets[f'trial_{(i*5)+j}']['pitches'] = trialPitches
            expOnsets[f'trial_{(i*5)+j}']['surprisals'] = trialSurprisals 
                
    print(overlapCount)

    with open(onsetsOutPath,'w+') as f:
        json.dump(expOnsets,f,indent=4)