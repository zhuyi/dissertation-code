import json
import os

subject = 'Conn'

def fix_blocks(subjectJSONPath,badSongs:list):
    with open(f'{subjectJSONPath}/blocks.json','r') as f:  
        trialsJSON = json.load(f)

    if badSongs:
        for i,blockKey in enumerate(trialsJSON):
            print(blockKey)
            for j,trialKey in enumerate(trialsJSON[blockKey]):
                if(trialKey == 'instrument'):continue
                trial = trialsJSON[blockKey][trialKey]
                
                print(trialKey)
                for segKey in trial:
                    segment = trial[segKey]
                    if segment['song_name'].split('.')[0] in badSongs:
                        segment['segment_start'] = 0
                        segment['segment_length'] = 0
                    
    with open(f'{subjectJSONPath}/good_blocks.json','w+') as f:  
        json.dump(trialsJSON,f,indent=4)