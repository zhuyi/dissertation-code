function sections = set_EEG_lengths(EEG_sections,wav_sections,lenRatio)
    nTrials = 30;
    sections = {};
    for i = 1:nTrials
        current_EEG_section = EEG_sections{i};
        trialLength = length(wav_sections{1,i})*lenRatio;
        if (trialLength > length(current_EEG_section))
            extraLen = trialLength - length(current_EEG_section);
            section = zeros(trialLength,size(current_EEG_section,2));
            section(1:length(current_EEG_section),:) = current_EEG_section;
            sections = [sections,section];
        else
            sections = [sections,current_EEG_section(1:trialLength,:)];
        end
    end
end