function tests = run_analysis(sub_num,eeg_step,eeg_len,name)
    addpath libs/cnsp_utils
    addpath libs/cnsp_utils/cnd
    addpath libs/mTRF-Toolbox_v2/mtrf
    addpath libs/NoiseTools
    addpath libs/eeglab

    if ~(exist("modelAll","var"))
        load("setup.mat");
    end
    if ~(exist("wavs","var"))
        wavs = load_all_wavs;
        disp(['Number of WAV structs loaded: ', num2str(length(wavs))]);
        lengths = {};
        for i = 1:length(wavs)
            lengths = [lengths,length(wavs{i}.data)];
        end
        disp(lengths);
    end

    disp(['Length of lengths: ', num2str(length(lengths))]);
    
    if ~(exist("altEnvelopes","var"))
        disp(lengths);
        [~,altEnvelopes] = load_all_onsets("../Data/MIDI/onsets/onsets.json",64,lengths);
    end
%%
    
    tests = {};
    eeg_fs = 64;
    for trial_num = 1:length(eeg_tests{sub_num}.data)
        for eeg_start = 1:eeg_step:(length(eeg_tests{sub_num}.data{trial_num})/64)-eeg_len
            eeg_end = eeg_start + eeg_len;
            
            [expected,expected_genre,instrument,overlap] = get_expected_label(name,eeg_tests{sub_num}.trial_nums(trial_num),eeg_start,eeg_end);
            
            if(~overlap)
                corrs = correlate_wavs(eeg_tests{sub_num}.data{trial_num}(eeg_start*eeg_fs:eeg_end*eeg_fs,:),modelAll(sub_num),altEnvelopes);
                ranks = get_most_correlated_wav(corrs,altEnvelopes,length(altEnvelopes));
                %disp(expected)
                %disp(name)
                %disp({ranks(:).name})
                %disp(eeg_tests{sub_num}.trial_nums(trial_num))
                test = {};
                test.expected = expected;
                test.expected_genre = expected_genre;
                test.instrument = instrument;
                test.ranks = ranks;
                tests = [tests,test];
            else
                disp('Overlap')
            end
        end
    end
    
   
end