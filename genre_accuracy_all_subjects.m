names = {"Subject_1","Subject_10","Subject_2","Subject_3","Subject_4","Subject_5","Subject_6","Subject_7","Subject_8","Subject_9"};

accuracies = {};

for i = 1:length(names)
    accuracy = genre_accuracy(allTests{i}, genreDict);
    accuracies{i} = accuracy;
end