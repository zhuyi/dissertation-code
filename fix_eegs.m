function fixedEEGs = fix_eegs(eegs,filename,changes)
    % Fixes EEGs with bad sections in them
    str = fileread(filename); 
    val = jsondecode(str);

    blockKeys = fieldnames(val);

    fixedEEGs = {};
    
    for i = 1:length(blockKeys)
        trialKeys = fieldnames(val.(blockKeys{i}));
        for j = 1:length(trialKeys)-1
            if(strcmp(trialKeys{j},'instrument'))
                continue
            end
            trial = val.(blockKeys{i}).(trialKeys{j});
            eeg = eegs{(5*(i-1)) + j};
            newEEG = [];
            segKeys = fieldnames(trial);
            prevChange = 1;
            trialChanges = changes{(5*(i-1)) + j};
            for k = 1:length(segKeys)
                segment = trial.(segKeys{k});
                if segment.segment_length ~= 0
                    newSeg = eeg(prevChange:trialChanges(k),:);
                    newEEG = cat(1,newEEG,newSeg);
                end
                prevChange = trialChanges(k);
            end
            fixedEEGs = [fixedEEGs,newEEG];
        end
    end
end