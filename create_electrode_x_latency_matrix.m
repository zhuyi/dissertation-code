% Load the extracted data
load('extracted_EEG_around_switches.mat', 'extractedData');

% Initialize the new structure for storing the averaged data
averagedData = struct('subjectData', cell(1, numSubjects));

% Loop over each subject
for subjectIndex = 1:10
    % Initialize a matrix to store the averaged data for the current subject
    subjectAveragedData = zeros(numElectrodes, prePostSamples * 2 + 1);
    
    % Loop over each electrode
    for electrodeIndex = 1:numElectrodes
        % Initialize a matrix to accumulate the values for averaging
        accumulatedData = zeros(prePostSamples * 2 + 1, 0);
        
        % Loop over each trial
        for trialIndex = 1:size(extractedData(subjectIndex).subjectData, 2)
            if ~isempty(extractedData(subjectIndex).subjectData{electrodeIndex, trialIndex})
                % Concatenate the segments for all switches within the current trial
                accumulatedData = [accumulatedData, extractedData(subjectIndex).subjectData{electrodeIndex, trialIndex}];
            end
        end
        
        % Calculate the average across all switches and trials for the current electrode
        if ~isempty(accumulatedData)
            subjectAveragedData(electrodeIndex, :) = mean(accumulatedData, 2);
        end
    end
    
    % Store the averaged data for the current subject in the new structure
    averagedData(subjectIndex).subjectData = subjectAveragedData;
end

% Save the averaged data to a new .mat file
save('averaged_EEG_around_switches.mat', 'averagedData');
