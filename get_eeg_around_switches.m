% Define the number of subjects and electrodes
numSubjects = 10;
numElectrodes = 64;

% Pre-allocate the structure to store the extracted segments
% Using a structure array with fields to hold the data in an organized manner
extractedData = struct('subjectData', cell(1, numSubjects));

% Sampling frequency and sample window parameters
fs = 64;
prePostSec = 5;
prePostSamples = prePostSec * fs;

for subjectIndex = 1:10
    % Load the subject's data file
    fileName = fullfile(sprintf('pre_dataSub%d_with_switches.mat', subjectIndex));
    if exist(fileName, 'file')
        load(fileName, 'eeg'); % Assuming 'eeg' is the variable name in the .mat file
    else
        warning('File does not exist: %s', fileName);
        continue; % Skip to the next subject if the file doesn't exist
    end
    
    % Initialize a cell array for the current subject's data
    subjectElectrodeData = cell(numElectrodes, 30);
    
    for trialIndex = 1:size(eeg.data, 2) % Iterate through each trial
        switchData = eeg.data{2, trialIndex};
        switchIndices = find(switchData == 1);
        
        for electrodeIndex = 1:numElectrodes
            % Initialize a matrix for storing segments for this electrode and trial
            % Assuming an unknown number of switches, start with an empty matrix
            electrodeSegments = [];
            
            for switchIdx = 1:length(switchIndices)
                startIndex = max(1, switchIndices(switchIdx) - prePostSamples);
                endIndex = min(size(eeg.data{1, trialIndex}, 1), switchIndices(switchIdx) + prePostSamples);
                
                % Ensure we have a uniform length for each segment
                segmentLength = endIndex - startIndex + 1;
                if segmentLength == (prePostSamples * 2 + 1)
                    segment = eeg.data{1, trialIndex}(startIndex:endIndex, electrodeIndex);
                    % Concatenate the segment as a new column in the matrix
                    electrodeSegments = [electrodeSegments, segment];
                else
                    % If segment does not meet expected length, pad or trim accordingly
                    % This scenario can occur if a switch is near the start or end of the data
                    segment = zeros(prePostSamples * 2 + 1, 1); % Initialize with zeros
                    actualSegment = eeg.data{1, trialIndex}(startIndex:endIndex, electrodeIndex);
                    padStart = prePostSamples - (switchIndices(switchIdx) - startIndex);
                    segment((padStart+1):(padStart+length(actualSegment))) = actualSegment;
                    % Concatenate the adjusted segment as a new column in the matrix
                    electrodeSegments = [electrodeSegments, segment];
                end
            end
            
            % Store the matrix of segments for this electrode and trial
            subjectElectrodeData{electrodeIndex, trialIndex} = electrodeSegments;
        end
    end
    
    % Store the data for the current subject in the structured array
    extractedData(subjectIndex).subjectData = subjectElectrodeData;
end

% Save the extracted data to a new .mat file
save('extracted_EEG_around_switches.mat', 'extractedData');
