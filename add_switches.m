for subjectIndex = 1:10
    % Load the pre_dataStim.mat file for the current subject
    stimFilePath = fullfile('..', 'Data', 'dataCND', ['pre_dataStim', num2str(subjectIndex), '.mat']);
    if exist(stimFilePath, 'file')
        load(stimFilePath); % Loads the 'stim' structure
    else
        disp(['File does not exist: ', stimFilePath]);
        continue; % Skip this subject if the file doesn't exist
    end

    % Load the pre_dataSub.mat file for the current subject
    subFilePath = fullfile('..', 'Data', 'dataCND', ['pre_dataSub', num2str(subjectIndex), '.mat']);
    if exist(subFilePath, 'file')
        load(subFilePath); % Loads the 'sub' structure
    else
        disp(['File does not exist: ', subFilePath]);
        continue; % Skip this subject if the file doesn't exist
    end

    % Define path to the good_blocks.json file for the current subject
    jsonFilePath = fullfile('..', 'melody-expectation-experiment', 'stimuli', 'subjects', ['subject_', num2str(subjectIndex)], 'good_blocks.json');

    % Load JSON data
    jsonText = fileread(jsonFilePath);
    jsonData = jsondecode(jsonText);

    fs = 64; % Sampling frequency

    % Process each block and trial contained in the good_blocks.json
    for blockIndex = 0:5
        blockName = sprintf('block_%d', blockIndex); % Corrected to match JSON structure
        if isfield(jsonData, blockName) % Check if the block exists
            currentBlock = jsonData.(blockName); % Use dynamic field names
            for trialIndex = 0:4
                trialName = sprintf('trial_%d', trialIndex);
                if isfield(currentBlock, trialName) % Check if the trial exists
                    trialData = currentBlock.(trialName);
                    totalSamples = 0; % Initialize total samples for this trial
                    
                    % Dynamic handling of segments since they're not in a 'segments' array
                    segmentNames = fieldnames(trialData);
                    for segIndex = 1:numel(segmentNames)
                        segment = trialData.(segmentNames{segIndex});
                        totalSamples = totalSamples + segment.segment_length / 1000;
                    end
                    
                    % Initialize switchData with zeros based on total samples
                    switchData = zeros(totalSamples * fs, 1);

                    currentSampleIndex = 1;
                    % Fill in switchData based on segment information
                    for segIndex = 1:numel(segmentNames)
                        segment = trialData.(segmentNames{segIndex});
                        if segIndex > 1 % Mark switch at the beginning of each segment, except the first
                            switchData(currentSampleIndex) = 1;
                        end
                        segmentSamples = segment.segment_length / 1000 * fs;
                        currentSampleIndex = currentSampleIndex + segmentSamples; % Move index
                    end

                    % Insert switchData into the appropriate cell in stim.data
                    stim.data{6, (blockIndex * 5) + trialIndex + 1} = switchData; % Adjust based on how trials are indexed
                    % Insert switchData into the appropriate cell in sub.data
                    eeg.data{2, (blockIndex * 5) + trialIndex + 1} = switchData; % Adjust based on how trials are indexed
                end
            end
        end
    end

    % Save the updated stim structure back to a new file specific to the subject
    save(['pre_dataStim', num2str(subjectIndex), '_with_switches.mat'], 'stim');
    % Save the updated sub structure back to a new file specific to the subject
    save(['pre_dataSub', num2str(subjectIndex), '_with_switches.mat'], 'eeg');
end
