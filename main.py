import json
import os
import random
from psychopy import visual, sound, event, core, parallel


from audioCombine import create_block

# Experiment Parameters
VOLUME = 0.2
FULL_SCREEN = False
RATIO_TEXT_SIZE = 40
RATIO_TEXT_WIDTH = 2
RATIO_CROSS = 40
CONTRAST = 0.1
BIOSEMI_TRIGGER = False
STIMS_PER_TRIAL = 6
TRIALS_PER_BLOCK = 5
BLOCKS = 6

COMPONENT_LOCATION = './stimuli/components'
OUTPUT_LOCATION = './stimuli/subjects'
BEHAVIOURAL_OUTPUT_LOCATION = './behavioural'
INSTRUMENTS = ['Piano','Sax']

NEUTRAL = 0
STIMULUS_START = 1
SONG_CHANGE = 2
BUTTON_PRESS = 3

PORT_ADDRESS = 0

def quit(win:visual.Window):
    win.close()
    core.quit()

# Allows for somewhat asynchronus handing of a button press
def handle_button_press(win:visual.window, globalClock:core.Clock, times):
    time = globalClock.getTime()
    if BIOSEMI_TRIGGER:
        parallel.setData(BUTTON_PRESS)
        win.callOnFlip(parallel.setData,NEUTRAL)
        win.flip()
    times.append(time)


# shows a slide of text on a given window
def show_slide(win, text: str):
    text = visual.TextStim(win=win, text=text, color="white", contrast=CONTRAST, wrapWidth=max(
        win.size) / RATIO_TEXT_WIDTH, height=max(win.size) / RATIO_TEXT_SIZE)
    text.autoDraw = True
    win.flip()
    event.waitKeys()
    text.autoDraw = False


def experiment(participantName: str, n_blocks: int):

    blocks = {}
    instruments = []

    # Step 1: generate what instrumet is going to be played
    for i in range(int(BLOCKS/len(INSTRUMENTS))):
        addedInstruments = random.sample(INSTRUMENTS,len(INSTRUMENTS))
        if instruments == []:
            instruments+= addedInstruments
            continue
        
        # Ensures that the same instrument isnt playing twice in a row
        while addedInstruments[0] == instruments[-1]:
            addedInstruments = random.sample(INSTRUMENTS,len(INSTRUMENTS))
        instruments += addedInstruments

    if BLOCKS%len(INSTRUMENTS) != 0:
        addedInstruments = random.sample(INSTRUMENTS,BLOCKS%len(INSTRUMENTS))
        
        while addedInstruments[0] == instruments[-1]:
            addedInstruments = random.sample(INSTRUMENTS,BLOCKS%len(INSTRUMENTS))
        instruments += addedInstruments

    print(instruments)

    # Generate blocks and save json detailing blocks to disk
    for i in range(n_blocks):
        out_path = os.path.join(OUTPUT_LOCATION,participantName,f'block_{i}')
        if not os.path.exists(out_path):
            os.makedirs(out_path)
        blocks[f'block_{i}'] = create_block(os.path.join(COMPONENT_LOCATION,instruments[i]),out_path,i,TRIALS_PER_BLOCK,STIMS_PER_TRIAL)
        blocks[f'block_{i}']['instrument'] = instruments[i]

    with open(os.path.join(OUTPUT_LOCATION,participantName,'blocks.json'),'w+') as outfile:
        json.dump(blocks,outfile,indent=4)

    # Main Experiment
    win = visual.Window(
        size=[800, 600],
        units="pix",
        fullscr=FULL_SCREEN,
        color=[-1, -1, -1]
    )
    win.mouseVisible = False

    fixation = visual.shape.ShapeStim(win=win,
                                      vertices="cross",
                                      color='white',
                                      fillColor='white',
                                      size=max(win.size) / RATIO_CROSS)
    fixation.contrast = CONTRAST

    parallel.setPortAddress(PORT_ADDRESS)  # where COM1 is the address of your port

    show_slide(win,"Good morning " + str(participantName) + "!\
	\n\nDuring this experiment, you will listen to sequences of melodies\
    \n\n When you think the melody has changed, press the a button.\
	\n\nPress any key on the keyboard to continue")

    buttonPresses = {}

    # Main experiment loop
    for i in range(n_blocks):
        blockFolderPath = os.path.join(OUTPUT_LOCATION,participantName,f'block_{str(i)}')
        #This gets all the paths for everything in the subdirs
        blockFolderSubPaths = [os.path.join(path, name) for path, subdirs, files in os.walk(blockFolderPath) for name in files]
        wavs = [val for val in blockFolderSubPaths if not val.endswith('.json')]
        jsons = [val for val in blockFolderSubPaths if val.endswith('.json')]
        trials = sorted(wavs)
        trialsjsons = sorted(jsons)

        trials = zip(trials,trialsjsons)

        blockPresses = {}

        # Main trial loop
        for j,trial in enumerate(trials):
            #wav and json are the paths
            wav = trial[0]
            jsonFile = trial[1]

            trialNum = (i*TRIALS_PER_BLOCK) + j

            show_slide(win,f"Trial {str(trialNum+1)} of {BLOCKS*TRIALS_PER_BLOCK}.\
                               \n\nWhen you're ready, press a key to listen!")
            fixation.autoDraw = True
            win.flip()

            audio = sound.Sound(wav,preBuffer=-1,volume=VOLUME)

            jsonData = json.load(open(jsonFile))

            #print(jsonData)

            times = []
            pressTimes = []

            for key in jsonData:
                times.append(jsonData[key]['segment_length'])

            #print(times)
            stimClock = core.Clock()

            event.globalKeys.add('a',func=handle_button_press,func_args=[win,stimClock,pressTimes])
            event.globalKeys.add(key='q', func=quit, func_args=[win])
            audio.play()
            stimClock.reset()

            for time in times:
                core.wait(time/1000,hogCPUperiod=time/1000)
                if BIOSEMI_TRIGGER:
                    parallel.setData(SONG_CHANGE)
                    win.callOnFlip(parallel.setData,NEUTRAL)
                    win.flip()

            fixation.autoDraw = False
            event.globalKeys.clear()
            blockPresses[f'trial_{j}'] = pressTimes

        buttonPresses[f'block_{i}'] = blockPresses

    # End Experiment
    show_slide(win, f"Thank you {str(participantName)} for your participation!\
	\n\nPress a key to end the experiment!")

    win.close()

    # Save the button press times
    if not os.path.exists(os.path.join(BEHAVIOURAL_OUTPUT_LOCATION,participantName)):
        os.makedirs(os.path.join(BEHAVIOURAL_OUTPUT_LOCATION,participantName))

    with open(os.path.join(BEHAVIOURAL_OUTPUT_LOCATION,participantName,f'buttonPresses.json'),'w+') as outfile:
        json.dump(buttonPresses,outfile,indent=4)


if __name__ == '__main__':
    print(f'Expected time: {BLOCKS*TRIALS_PER_BLOCK*(STIMS_PER_TRIAL/3)} mins')
    print(f'Changes in genre: {BLOCKS*(STIMS_PER_TRIAL-1)*TRIALS_PER_BLOCK}')
    input('Is this ok?')
    experiment('test', BLOCKS)