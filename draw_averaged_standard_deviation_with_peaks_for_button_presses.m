% Load the standard deviation data
load('std_deviation_EEG_around_button_presses.mat', 'stdDeviationData');

% Number of subjects, electrodes, and latencies
numSubjects = length(stdDeviationData);
numLatencies = 641; % prePostSamples * 2 + 1

% Initialize a matrix to accumulate the standard deviation data for averaging
allSubjectsStdDeviation = zeros(numSubjects, numLatencies);

% Collect data from each subject
for subjectIndex = 1:numSubjects
    % Extract the standard deviation data for the current subject
    subjectStdDeviation = stdDeviationData(subjectIndex).subjectStdDev;
    
    % Ensure the subject data is in the expected format
    if length(subjectStdDeviation) ~= numLatencies
        error('Unexpected data format for subject %d. Expected %d latencies, found %d.', ...
              subjectIndex, numLatencies, length(subjectStdDeviation));
    end
    
    % Store the standard deviation data in the matrix
    allSubjectsStdDeviation(subjectIndex, :) = subjectStdDeviation;
end

% Calculate the average standard deviation across all subjects
averageStdDeviation = mean(allSubjectsStdDeviation, 1);

% Define time vector for plotting (assuming -5 to +5 seconds)
timeVector = linspace(-5, 5, numLatencies);

% Find peaks in the averaged standard deviation data
[peakValues, peakIndices] = findpeaks(averageStdDeviation);

% Find the highest peak after time 0
postZeroPeaks = timeVector(peakIndices) > 0;
postZeroPeakValues = peakValues(postZeroPeaks);
postZeroPeakIndices = peakIndices(postZeroPeaks);
[~, highestPostZeroIndex] = max(postZeroPeakValues);
highlightIndex2 = postZeroPeakIndices(highestPostZeroIndex);

% Highlight times and values
highlightTimes = [0, timeVector(highlightIndex2)];
highlightValues = [averageStdDeviation(timeVector == 0), averageStdDeviation(highlightIndex2)];

% Plot the average standard deviation and mark the specified points
figure;
plot(timeVector, averageStdDeviation, 'LineWidth', 2);
hold on;
plot(highlightTimes, highlightValues, 'ro', 'MarkerSize', 10, 'LineWidth', 2);
title('Average Standard Deviation of EEG Signals Around Button Presses Across All Subjects');
xlabel('Time (seconds)');
ylabel('Global Field Power');
grid on;
xticks(linspace(-5, 5, 11)); % Set ticks at -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5
legend('Average Std Dev', 'Highlighted Peaks');
hold off;

% Save the figure in the plots folder
plotsFolder = 'EEG_StdDeviation_Plots_ButtonPresses';
if ~exist(plotsFolder, 'dir')
    mkdir(plotsFolder);
end
averagePlotFileName = fullfile(plotsFolder, 'Average_EEG_StdDeviation_with_HighlightedPeaks.png');
saveas(gcf, averagePlotFileName);

% Close the figure to avoid too many open figures
close(gcf);

disp('Plot with highlighted peaks created and saved successfully.');

% Display the identified peak times
disp('Identified Peaks:');
disp(table(highlightTimes', highlightValues', 'VariableNames', {'Time', 'Value'}));
