names = {"Subject_1","Subject_2","Subject_3","Subject_4"};

accuracies = {};

for i = 1:length(names)
    accuracy = top_5_accuracy(allTests{i});
    accuracies{i} = accuracy;
end