addpath('miditoolbox/');

% Define the MIDI files for the genres
daftpunkFiles = {'Aerodynamic.mid', 'Contact.mid', 'Derezzed.mid', 'DigitalLove.mid', 'GiorgioByMoroder.mid', 'HarderBetterFasterStronger.mid', 'ShortCircuit.mid', 'VeridisQuo.mid'};
bachFiles = {'Bach Sonata 3 Fugue.mid', 'Bach Suite 3 Allemande.mid', 'Bach Suite 3 Bourree.mid'};
jazzFiles = {'BlackPearls.mid', 'Chameleon Bass Riff.mid', 'Chameleon Sax Riff.mid', 'I Mean You Main Melody.mid', 'One Finger Snap.mid'};
popFiles = {'HotelCalifornia.mid', 'letsdance.mid', 'Thriller.mid', 'WhiteWedding.mid'};

% Group the files by genre
genres = {daftpunkFiles, bachFiles, jazzFiles, popFiles};
genreNames = {'DaftPunk', 'Bach', 'Jazz', 'Pop'};

% Loop over each genre
for genreIndex = 1:length(genres)
    midiFiles = genres{genreIndex}; % Current genre's MIDI files
    totalAverageDistance = 0;
    
    % Loop over the list of MIDI files for the current genre
    for fileIndex = 1:length(midiFiles)
        % Construct the full path to the MIDI file
        filePath = fullfile('../Data/MIDI', midiFiles{fileIndex});
        
        % Read the MIDI file
        midiData = readmidi(filePath);
        
        % Calculate the average distance between notes for this file
        averageDistance = mean(diff(midiData(:, 6)));
        
        % Accumulate the average distance
        totalAverageDistance = totalAverageDistance + averageDistance;
        
        % Display the average distance for each MIDI file
        disp(['Average distance between notes for ', midiFiles{fileIndex}, ': ', num2str(averageDistance), ' seconds']);
    end
    
    % Calculate the overall average distance across all MIDI files for the current genre
    averageDistanceGenre = totalAverageDistance / length(midiFiles);
    
    % Display the overall average distance for the current genre
    disp(['Overall average distance between notes - ', genreNames{genreIndex}, ': ', num2str(averageDistanceGenre), ' seconds']);
end
